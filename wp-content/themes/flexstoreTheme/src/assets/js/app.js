import slider from "./partials/slider";
import modals from "./partials/modals";
import gallery from "./partials/gallery";
import accessories from "./partials/accessories";
import system from "./partials/system";
import forms from "./partials/forms";
window.$ = $;
export const loading = `<div class="loading">Loading...</div>`;

let openMenu = $(".open-mobile-menu"),
  closeMenu = $(".close-mobile-menu"),
  menu = $(".site-header__actions");

slider();
modals();
gallery();
accessories();
forms();
system();

openMenu.on("click", function(event) {
  event.preventDefault();
  menu.addClass("out");
});
closeMenu.on("click", function(event) {
  event.preventDefault();
  menu.removeClass("out");
});
