function forms() {
  let formGroup = $(".form-group");
  formGroup.on("click", function() {
    let input = $(this).find("input");
    input.focus();
  });
}
export default forms;
