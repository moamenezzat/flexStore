import { loading } from "../app";

function makeModalImage(image, first) {
  return `<div class="gallery-page__modal__image ${first ? "active" : ""}">
    <img src="${image.url}" alt="${image.name}"></div>`;
}

function gallery() {
  let galleryFilters = $(".gallery-page__filter");
  if (galleryFilters.length) {
    let imagesContainer = $(".gallery-page__images"),
      galleryImages = $(".gallery-page__image"),
      modalImagesTrack = $(".gallery-page__modal__images-track"),
      modalCaptions = $(".gallery-page__modal__caption"),
      modalCountFrom = $(".gallery-page__modal__count .current"),
      modalCountTo = $(".gallery-page__modal__count .total"),
      controls = $(".gallery-page__modal__control"),
      prev = $(".gallery-page__modal__control.prev"),
      next = $(".gallery-page__modal__control.next"),
      modalImages = $(".gallery-page__modal__image"),
      modalImagesNumber = modalImages.length,
      galleryModal = $(".media-modal"),
      galleryModalImageWidth = $(".gallery-page__modal__image").outerWidth(),
      info = imagesContainer.data("info"),
      currentFilter = "",
      openedModal = false,
      mobileFiltersBtn = $(".gallery-page__filters-expand-btn"),
      filtersContainer = $(".gallery-page__filter-container");
    function gotoSlide(index) {
      let activeImage = $(".gallery-page__modal__image.active");
      activeImage.removeClass("active");
      modalImagesTrack.css(
        "transform",
        `translateX(${-index * galleryModalImageWidth}px)`
      );
      modalCaptions.html(info[index].name);
      modalCountFrom.html(index + 1);
      modalImages.eq(index).addClass("active");

      controls.removeClass("disabled");
      if (index + 1 === modalImagesNumber) {
        next.addClass("disabled");
      }
      if (index === 0) {
        prev.addClass("disabled");
      }
    }

    imagesContainer.on("click", ".gallery-page__image", function() {
      let $this = $(this),
        index = galleryImages.index($this);
      if (!openedModal) {
        let images = "";
        for (var image in info) {
          images += makeModalImage(info[image], image === 0 ? true : false);
        }
        modalImagesTrack.html(images);
        modalImages = $(".gallery-page__modal__image");
        modalImagesNumber = modalImages.length;
        modalCountFrom.html(index + 1);
        modalCountTo.html(modalImagesNumber);
        openedModal = true;
      }
      gotoSlide(index);
      galleryModal.fadeIn(500);
    });

    galleryFilters.on("click", function() {
      let $this = $(this),
        activeFilter = $(".gallery-page__filter.active");
      if (!$this.hasClass("active")) {
        let filter = $this.data("cat");
        imagesContainer.html(loading);
        $.get(`/gallery/?filter=${filter}`).then(resp => {
          let $resp = $(resp),
            newInfo = $resp.find(".gallery-page__images").data("info");
          info = newInfo;
          currentFilter = filter;
          imagesContainer.html($resp.find(".gallery-page__image"));
          imagesContainer.data("info", newInfo);
          activeFilter.removeClass("active");
          $this.addClass("active");
          openedModal = false;
          galleryImages = $(".gallery-page__image");
        });
      }
    });

    controls.on("click", function() {
      let $this = $(this),
        index = modalImages.index($(".gallery-page__modal__image.active"));
      if ($this.hasClass("next")) {
        if (index < modalImagesNumber - 1) {
          gotoSlide(index + 1);
          index + 1 === modalImagesNumber - 1
            ? $this.addClass("disabled")
            : controls.removeClass("disabled");
        }
      } else if ($this.hasClass("prev")) {
        if (index > 0) {
          gotoSlide(index - 1);
        }
      }
    });
    mobileFiltersBtn.on("click", function(event) {
      event.preventDefault();
      let $this = $(this),
        height;
      if (!filtersContainer.data("height")) {
        height = filtersContainer[0].scrollHeight;
        filtersContainer.data("height", height);
      } else {
        height = filtersContainer.data("height");
      }
      if (filtersContainer.hasClass("open")) {
        filtersContainer.css("max-height", 0);
      } else {
        filtersContainer.css("max-height", `${height}px`);
      }
      filtersContainer.toggleClass("open");
    });
  }
}
export default gallery;
