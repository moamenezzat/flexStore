function modals() {
  let triggers = $("[data-open-modal]"),
    closeModal = $("[data-close-modal]"),
    body = $("body");
  triggers.on("click", function() {
    let $this = $(this),
      target = $this.data("modal"),
      modal = $(`.media-modal[data-modal="${target}"]`);

    modal.fadeIn("fast", function() {
      if (target === "video") {
        let id = $this.data("id"),
          source = $this.data("source"),
          video = modal.find(".media-modal__video");
        if (!video.attr("src")) {
          let url = "";
          if (source === "youtube") {
            url = `https://www.youtube.com/embed/${id}?rel=0&showinfo=0&autoplay=1`;
          } else if (source === "wistia") {
            url = `//fast.wistia.net/embed/iframe/${id}?videoFoam=true&autoPlay=true&playerColor=fb0001`;
          } else if (source === "vimeo") {
            url = `https://player.vimeo.com/video/${id}?autoplay=1&loop=1&autopause=0`;
          }
          video.attr("src", url);
        }
        // video[0].play();
      }
    });
  });
  closeModal.on("click", function() {
    let $this = $(this),
      modal = $this.parents(`.media-modal`),
      type = modal.data("modal");
    if (type === "video") {
      modal.find(".media-modal__video").attr("src", "");
    }
    modal.fadeOut("fase", function() {
      body.removeClass("no-scroll");
    });
  });
}
export default modals;
