function system() {
  let images = $(".system-page__gallery__image"),
    imagesNumber = images.length,
    modelsTables = $(".system-page__models__table");
  if (imagesNumber) {
    let controls = $(".system-page__gallery__control");
    controls.on("click", function() {
      let $this = $(this),
        activeImage = $(".system-page__gallery__image.active"),
        index = images.index(activeImage);

      if ($this.hasClass("next")) {
        if (index < imagesNumber - 1) {
          images.eq(index + 1).addClass("active");
        } else {
          images.eq(0).addClass("active");
        }
      } else {
        if (index === 0) {
          images.eq(imagesNumber - 1).addClass("active");
        } else {
          images.eq(index - 1).addClass("active");
        }
      }
      activeImage.removeClass("active");
    });
  }
  if (modelsTables.length) {
    let height = modelsTables.eq(0).height();
    modelsTables.eq(1).height(height);
  }
}

export default system;
