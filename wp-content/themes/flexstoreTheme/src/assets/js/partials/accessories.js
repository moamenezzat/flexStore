function accessories() {
  let accessoryModalTriggers = $("[data-open-accessory-modal]");
  if (accessoryModalTriggers.length) {
    let modal = $(".accessories-modal"),
      modalImage = $(".accessories-modal__image"),
      modalTitle = $(".accessories-modal__title"),
      modalFeatures = $(".accessories-modal__features"),
      modalDesc = $(".accessories-modal__desc"),
      body = $("body");

    accessoryModalTriggers.on("click", function() {
      let accessory = $(this).parents(".accessory"),
        accessoryTitle = accessory.find(".accessory__title"),
        accessoryImage = accessory.find(".accessory__image"),
        accessoryFeatures = accessory.find(".accessory__features"),
        accessoryDesc = accessory.find(".accessory__desc");
      modalImage.attr({
        src: accessoryImage.attr("src"),
        alt: accessoryImage.attr("alt")
      });
      modalTitle.html(accessoryTitle.html());
      modalFeatures.html(accessoryFeatures.html());
      modalDesc.html(accessoryDesc.html());
      modal.fadeIn(500, () => {
        body.addClass("no-scroll");
      });
    });
  }
}

export default accessories;
