function slider() {
  let slider = $(".customers__slider");

  function getTranslateX(myElement) {
    let style = window.getComputedStyle(myElement),
      matrix = new WebKitCSSMatrix(style.webkitTransform);
    return matrix.m41;
  }

  if (slider.length) {
    let slides = $(".customers__slider__slide"),
      slidesContainer = $(".customers__slider__slide-container"),
      width = slidesContainer[0].scrollWidth,
      slidesNumber = slides.length,
      duration = slidesNumber * 5;
    slidesContainer.css("animation-duration", `${duration}s`);
    slidesContainer.css("width", `${width}px`);
    slidesContainer.addClass("marquee");
    slidesContainer.addClass("play");

    let currentX, active, intialMousePosition, direction, timer;

    slidesContainer.on("touchstart", dragStart);
    slidesContainer.on("touchend", dragEnd);
    slidesContainer.on("touchmove", drag);

    slidesContainer.on("mousedown", dragStart);
    slidesContainer.on("mouseup", dragEnd);
    slidesContainer.on("mouseleave", mouseLeave);
    slidesContainer.on("mousemove", drag);

    function dragStart(e) {
      clearInterval(timer);
      e.preventDefault();
      e.stopPropagation();
      slidesContainer.removeClass("play");
      slidesContainer.removeClass("finish");
      slidesContainer.removeClass("start");
      currentX = getTranslateX(slidesContainer[0]);
      slidesContainer.css("transform", `translateX(${currentX}px)`);
      slidesContainer.removeClass("marquee");
      slidesContainer.removeClass("marquee-back");
      active = true;
      if (e.type === "touchstart") {
        intialMousePosition = e.touches[0].clientX;
      } else {
        intialMousePosition = e.clientX;
      }
    }

    function dragEnd(e) {
      e.preventDefault();
      e.stopPropagation();
      if (active) {
        active = false;
        let finalPosition =
            getTranslateX(slidesContainer[0]) > -10
              ? 0
              : getTranslateX(slidesContainer[0]),
          percentage = -Math.round((finalPosition * 100) / width);
        if (finalPosition < 0) {
          if (direction === "left") {
            var transitionDuration =
                direction === "left"
                  ? (1 - percentage / 100) * duration
                  : (percentage * duration) / 100,
              frame =
                ((1 - percentage / 100) * width) /
                ((transitionDuration * 1000) / 16);
          } else {
            var transitionDuration = (percentage * duration) / 100,
              frame =
                (percentage * width) / 100 / ((transitionDuration * 1000) / 16);
          }
        } else {
          slidesContainer.css("transform", `translateX(${0}px)`);
        }

        if (direction === "left") {
          timer = setInterval(function() {
            let curentPosition = getTranslateX(slidesContainer[0]);
            if (-curentPosition >= width) {
              clearInterval(timer);
              slidesContainer.addClass("marquee-back");
              slidesContainer.addClass("play");
              return;
            } else {
              slidesContainer.css(
                "transform",
                `translateX(${curentPosition - frame}px)`
              );
            }
          }, 16);
        } else {
          timer = setInterval(function() {
            let curentPosition = getTranslateX(slidesContainer[0]);
            if (curentPosition >= 0) {
              clearInterval(timer);
              slidesContainer.addClass("marquee");
              slidesContainer.addClass("play");
              return;
            } else {
              slidesContainer.css(
                "transform",
                `translateX(${frame + curentPosition}px)`
              );
            }
          }, 16);
        }
        slidesContainer.off("transitionend");
        slidesContainer.on("transitionend", function() {
          slidesContainer.removeClass("finish");
          slidesContainer.removeClass("start");
          slidesContainer.css("transition-duration", `${0}s`);
          if (direction === "right") {
            slidesContainer.addClass("marquee");
          } else {
            slidesContainer.addClass("marquee-back");
          }
          slidesContainer.addClass("play");
        });
      }
    }
    function mouseLeave(e) {
      e.preventDefault();
      e.stopPropagation();
      if (active) {
        slidesContainer.trigger("mouseup");
      }
    }

    function drag(e) {
      e.preventDefault();
      e.stopPropagation();
      let dragX;
      if (active) {
        if (e.type === "touchmove") {
          e.preventDefault();
          let element = $(
            document.elementFromPoint(
              e.touches[0].clientX,
              e.touches[0].clientY
            )
          );
          if (!element.is(slidesContainer)) {
            slidesContainer.trigger("touchend");
          }
          dragX = e.touches[0].clientX;
        } else {
          dragX = e.clientX;
        }
        let delta = -dragX + intialMousePosition,
          position = currentX - delta;
        if (dragX > intialMousePosition) {
          direction = "right";
        } else {
          direction = "left";
        }
        if (position < 0 && position > -width)
          slidesContainer.css("transform", `translateX(${position}px)`);
      } else {
        dragEnd({
          clientX: 0,
          preventDefault() {},
          stopPropagation() {}
        });
      }
    }
  }
}
export default slider;
