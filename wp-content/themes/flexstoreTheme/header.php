<?php

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>

	<header class="site-header flex-container align-center" role="banner">
		<a href="/" class="logo">
			<img src="<?php echo get_template_directory_uri() ; ?>/dist/assets/images/FlexStor.svg" alt="FlexStore">
		</a>

		<div class="site-header__actions align-center flex-container">
			<button class="close-mobile-menu" data-toggle-menu><img src="<?php echo get_template_directory_uri() ; ?>/dist/assets/images/icons/close-menu.svg" alt="close menu icon"></button>
			<?php wp_nav_menu( array(
			    'menu' => 'Main',
					'container' =>'',
			) ); ?>
			<a class="cta" href="/quote">
				<span>Request a Quote</span>
			</a>
		</div>
		<button class="open-mobile-menu" data-toggle-menu>
			<span></span>
			<span></span>
			<span></span>
		</button>
	</header>
