
<footer class="footer">
  <div class="footer__logo">
    <img src="<?php echo get_template_directory_uri() ; ?>/dist/assets/images/FlexStor.svg" alt="FlexStore">
    <p>Flex Essentials Inc.</p>
  </div>
  <div class="flex-container flex-start footer__main-content">
    <div class="footer__address-container">
      <p class="footer__address">17769 Centreville Creek Road <br> Caledon East, ON L7K 2L9 <br> Canada</p>
      <p class="footer__contact-info">
        1-905-584-6369
      </p>
    </div>
    <div class="flex-container footer__actions flex-start space-between">
      <?php wp_nav_menu( array(
          'menu' => 'footer',
          'container' =>'',
          'menu_class' => 'footer__menu'
      ) ); ?>
      <div class="footer__resources">
        <a href="" class="footer__resource file" rel="noopener noreferrer" target="_blank"><?php echo do_shortcode("[replace_logo logo=FlexStor]") ?> Brochure</a>
        <a href="" class="footer__resource file" rel="noopener noreferrer" target="_blank"><?php echo do_shortcode("[replace_logo logo=FlexStand]") ?> Brochure</a>
        <a href="" class="footer__resource link" rel="noopener noreferrer" target="_blank"><?php echo do_shortcode("[replace_logo logo=FlexStor]") ?> Detectors</a>
        <a href="" class="footer__resource link" rel="noopener noreferrer" target="_blank">Polywest Flexo Sleeves</a>
      </div>
    </div>
    <div class="footer__shipping">
        <p class="footer__shipping__quote">
          We’re proud to provide international shipping.
          <br>Please contact your local representative for more information.
        </p>
        <a href="" class="inline-link">
          Learn more
       </a>
      </div>
  </div>
  <p class="footer__copyright">Copyright © FlexStor <?php echo date("Y"); ?></p>
</footer>
<?php wp_footer(); ?>

</body>
</html>
