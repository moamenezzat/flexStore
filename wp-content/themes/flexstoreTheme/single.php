<?php
  get_header();
?>
<section class="page-header news-archive">
  <h1 class="page-title">
    <?php the_title(); ?>
  </h1>
   <p class="news-article__date"> Posted on <strong><?php echo get_the_date(); ?></strong></p>
</section>
<img src="<?php echo get_the_post_thumbnail_url();?>" alt="<?php the_title(); ?> poster" class="news-page__img">
<div class="news-page__content">
  <?php while ( have_posts() ) : the_post(); ?>
    <?php 		the_content();?>
  <?php endwhile;  wp_reset_query(); ?>
</div>
<?php
$posts = get_field('related_posts');
if( $posts ){
 ?>
<div class="news-page__related-posts">
    <h2 class="section-title">Related News</h2>
    <div class="flex-container">
    <?php
    foreach( $posts as $post) {
      setup_postdata($post);?>
      <a href="<?php the_permalink(); ?>" class="news-article">
        <div class="news-article__img" style="background-image: url('<?php echo get_the_post_thumbnail_url();?>')"></div>
        <h2 class="news-article__title">
          <?php the_title(); ?>
        </h2>
        <p class="news-article__date"> Posted on <strong><?php echo get_the_date(); ?></strong></p>
      </a>
    <?php } ?>
    </div>
<?php
  wp_reset_query(); ?>
</div>
<?php } ?>
<a href="/quote" class="cta full-width "><span>Request a Free Quote</span></a>

<?php get_footer();
