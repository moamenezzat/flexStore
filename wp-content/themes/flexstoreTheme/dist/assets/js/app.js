/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loading = undefined;

var _slider = __webpack_require__(2);

var _slider2 = _interopRequireDefault(_slider);

var _modals = __webpack_require__(3);

var _modals2 = _interopRequireDefault(_modals);

var _gallery = __webpack_require__(4);

var _gallery2 = _interopRequireDefault(_gallery);

var _accessories = __webpack_require__(5);

var _accessories2 = _interopRequireDefault(_accessories);

var _system = __webpack_require__(6);

var _system2 = _interopRequireDefault(_system);

var _forms = __webpack_require__(7);

var _forms2 = _interopRequireDefault(_forms);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

window.$ = $;
var loading = exports.loading = "<div class=\"loading\">Loading...</div>";

var openMenu = $(".open-mobile-menu"),
    closeMenu = $(".close-mobile-menu"),
    menu = $(".site-header__actions");

(0, _slider2.default)();
(0, _modals2.default)();
(0, _gallery2.default)();
(0, _accessories2.default)();
(0, _forms2.default)();
(0, _system2.default)();

openMenu.on("click", function (event) {
  event.preventDefault();
  menu.addClass("out");
});
closeMenu.on("click", function (event) {
  event.preventDefault();
  menu.removeClass("out");
});

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(0);


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
function slider() {
  var slider = $(".customers__slider");

  function getTranslateX(myElement) {
    var style = window.getComputedStyle(myElement),
        matrix = new WebKitCSSMatrix(style.webkitTransform);
    return matrix.m41;
  }

  if (slider.length) {
    var dragStart = function dragStart(e) {
      clearInterval(timer);
      e.preventDefault();
      e.stopPropagation();
      slidesContainer.removeClass("play");
      slidesContainer.removeClass("finish");
      slidesContainer.removeClass("start");
      currentX = getTranslateX(slidesContainer[0]);
      slidesContainer.css("transform", "translateX(" + currentX + "px)");
      slidesContainer.removeClass("marquee");
      slidesContainer.removeClass("marquee-back");
      active = true;
      if (e.type === "touchstart") {
        intialMousePosition = e.touches[0].clientX;
      } else {
        intialMousePosition = e.clientX;
      }
    };

    var dragEnd = function dragEnd(e) {
      e.preventDefault();
      e.stopPropagation();
      if (active) {
        active = false;
        var finalPosition = getTranslateX(slidesContainer[0]) > -10 ? 0 : getTranslateX(slidesContainer[0]),
            percentage = -Math.round(finalPosition * 100 / width);
        if (finalPosition < 0) {
          if (direction === "left") {
            var transitionDuration = direction === "left" ? (1 - percentage / 100) * duration : percentage * duration / 100,
                frame = (1 - percentage / 100) * width / (transitionDuration * 1000 / 16);
          } else {
            var transitionDuration = percentage * duration / 100,
                frame = percentage * width / 100 / (transitionDuration * 1000 / 16);
          }
        } else {
          slidesContainer.css("transform", "translateX(" + 0 + "px)");
        }

        if (direction === "left") {
          timer = setInterval(function () {
            var curentPosition = getTranslateX(slidesContainer[0]);
            if (-curentPosition >= width) {
              clearInterval(timer);
              slidesContainer.addClass("marquee-back");
              slidesContainer.addClass("play");
              return;
            } else {
              slidesContainer.css("transform", "translateX(" + (curentPosition - frame) + "px)");
            }
          }, 16);
        } else {
          timer = setInterval(function () {
            var curentPosition = getTranslateX(slidesContainer[0]);
            if (curentPosition >= 0) {
              clearInterval(timer);
              slidesContainer.addClass("marquee");
              slidesContainer.addClass("play");
              return;
            } else {
              slidesContainer.css("transform", "translateX(" + (frame + curentPosition) + "px)");
            }
          }, 16);
        }
        slidesContainer.off("transitionend");
        slidesContainer.on("transitionend", function () {
          slidesContainer.removeClass("finish");
          slidesContainer.removeClass("start");
          slidesContainer.css("transition-duration", 0 + "s");
          if (direction === "right") {
            slidesContainer.addClass("marquee");
          } else {
            slidesContainer.addClass("marquee-back");
          }
          slidesContainer.addClass("play");
        });
      }
    };

    var mouseLeave = function mouseLeave(e) {
      e.preventDefault();
      e.stopPropagation();
      if (active) {
        slidesContainer.trigger("mouseup");
      }
    };

    var drag = function drag(e) {
      e.preventDefault();
      e.stopPropagation();
      var dragX = void 0;
      if (active) {
        if (e.type === "touchmove") {
          e.preventDefault();
          var element = $(document.elementFromPoint(e.touches[0].clientX, e.touches[0].clientY));
          if (!element.is(slidesContainer)) {
            slidesContainer.trigger("touchend");
          }
          dragX = e.touches[0].clientX;
        } else {
          dragX = e.clientX;
        }
        var delta = -dragX + intialMousePosition,
            position = currentX - delta;
        if (dragX > intialMousePosition) {
          direction = "right";
        } else {
          direction = "left";
        }
        if (position < 0 && position > -width) slidesContainer.css("transform", "translateX(" + position + "px)");
      } else {
        dragEnd({
          clientX: 0,
          preventDefault: function preventDefault() {},
          stopPropagation: function stopPropagation() {}
        });
      }
    };

    var slides = $(".customers__slider__slide"),
        slidesContainer = $(".customers__slider__slide-container"),
        width = slidesContainer[0].scrollWidth,
        slidesNumber = slides.length,
        duration = slidesNumber * 5;
    slidesContainer.css("animation-duration", duration + "s");
    slidesContainer.css("width", width + "px");
    slidesContainer.addClass("marquee");
    slidesContainer.addClass("play");

    var currentX = void 0,
        active = void 0,
        intialMousePosition = void 0,
        direction = void 0,
        timer = void 0;

    slidesContainer.on("touchstart", dragStart);
    slidesContainer.on("touchend", dragEnd);
    slidesContainer.on("touchmove", drag);

    slidesContainer.on("mousedown", dragStart);
    slidesContainer.on("mouseup", dragEnd);
    slidesContainer.on("mouseleave", mouseLeave);
    slidesContainer.on("mousemove", drag);
  }
}
exports.default = slider;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
function modals() {
  var triggers = $("[data-open-modal]"),
      closeModal = $("[data-close-modal]"),
      body = $("body");
  triggers.on("click", function () {
    var $this = $(this),
        target = $this.data("modal"),
        modal = $(".media-modal[data-modal=\"" + target + "\"]");

    modal.fadeIn("fast", function () {
      if (target === "video") {
        var id = $this.data("id"),
            source = $this.data("source"),
            video = modal.find(".media-modal__video");
        if (!video.attr("src")) {
          var url = "";
          if (source === "youtube") {
            url = "https://www.youtube.com/embed/" + id + "?rel=0&showinfo=0&autoplay=1";
          } else if (source === "wistia") {
            url = "//fast.wistia.net/embed/iframe/" + id + "?videoFoam=true&autoPlay=true&playerColor=fb0001";
          } else if (source === "vimeo") {
            url = "https://player.vimeo.com/video/" + id + "?autoplay=1&loop=1&autopause=0";
          }
          video.attr("src", url);
        }
        // video[0].play();
      }
    });
  });
  closeModal.on("click", function () {
    var $this = $(this),
        modal = $this.parents(".media-modal"),
        type = modal.data("modal");
    if (type === "video") {
      modal.find(".media-modal__video").attr("src", "");
    }
    modal.fadeOut("fase", function () {
      body.removeClass("no-scroll");
    });
  });
}
exports.default = modals;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _app = __webpack_require__(0);

function makeModalImage(image, first) {
  return "<div class=\"gallery-page__modal__image " + (first ? "active" : "") + "\">\n    <img src=\"" + image.url + "\" alt=\"" + image.name + "\"></div>";
}

function gallery() {
  var galleryFilters = $(".gallery-page__filter");
  if (galleryFilters.length) {
    var gotoSlide = function gotoSlide(index) {
      var activeImage = $(".gallery-page__modal__image.active");
      activeImage.removeClass("active");
      modalImagesTrack.css("transform", "translateX(" + -index * galleryModalImageWidth + "px)");
      modalCaptions.html(info[index].name);
      modalCountFrom.html(index + 1);
      modalImages.eq(index).addClass("active");

      controls.removeClass("disabled");
      if (index + 1 === modalImagesNumber) {
        next.addClass("disabled");
      }
      if (index === 0) {
        prev.addClass("disabled");
      }
    };

    var imagesContainer = $(".gallery-page__images"),
        galleryImages = $(".gallery-page__image"),
        modalImagesTrack = $(".gallery-page__modal__images-track"),
        modalCaptions = $(".gallery-page__modal__caption"),
        modalCountFrom = $(".gallery-page__modal__count .current"),
        modalCountTo = $(".gallery-page__modal__count .total"),
        controls = $(".gallery-page__modal__control"),
        prev = $(".gallery-page__modal__control.prev"),
        next = $(".gallery-page__modal__control.next"),
        modalImages = $(".gallery-page__modal__image"),
        modalImagesNumber = modalImages.length,
        galleryModal = $(".media-modal"),
        galleryModalImageWidth = $(".gallery-page__modal__image").outerWidth(),
        info = imagesContainer.data("info"),
        currentFilter = "",
        openedModal = false,
        mobileFiltersBtn = $(".gallery-page__filters-expand-btn"),
        filtersContainer = $(".gallery-page__filter-container");


    imagesContainer.on("click", ".gallery-page__image", function () {
      var $this = $(this),
          index = galleryImages.index($this);
      if (!openedModal) {
        var images = "";
        for (var image in info) {
          images += makeModalImage(info[image], image === 0 ? true : false);
        }
        modalImagesTrack.html(images);
        modalImages = $(".gallery-page__modal__image");
        modalImagesNumber = modalImages.length;
        modalCountFrom.html(index + 1);
        modalCountTo.html(modalImagesNumber);
        openedModal = true;
      }
      gotoSlide(index);
      galleryModal.fadeIn(500);
    });

    galleryFilters.on("click", function () {
      var $this = $(this),
          activeFilter = $(".gallery-page__filter.active");
      if (!$this.hasClass("active")) {
        var filter = $this.data("cat");
        imagesContainer.html(_app.loading);
        $.get("/gallery/?filter=" + filter).then(function (resp) {
          var $resp = $(resp),
              newInfo = $resp.find(".gallery-page__images").data("info");
          info = newInfo;
          currentFilter = filter;
          imagesContainer.html($resp.find(".gallery-page__image"));
          imagesContainer.data("info", newInfo);
          activeFilter.removeClass("active");
          $this.addClass("active");
          openedModal = false;
          galleryImages = $(".gallery-page__image");
        });
      }
    });

    controls.on("click", function () {
      var $this = $(this),
          index = modalImages.index($(".gallery-page__modal__image.active"));
      if ($this.hasClass("next")) {
        if (index < modalImagesNumber - 1) {
          gotoSlide(index + 1);
          index + 1 === modalImagesNumber - 1 ? $this.addClass("disabled") : controls.removeClass("disabled");
        }
      } else if ($this.hasClass("prev")) {
        if (index > 0) {
          gotoSlide(index - 1);
        }
      }
    });
    mobileFiltersBtn.on("click", function (event) {
      event.preventDefault();
      var $this = $(this),
          height = void 0;
      if (!filtersContainer.data("height")) {
        height = filtersContainer[0].scrollHeight;
        filtersContainer.data("height", height);
      } else {
        height = filtersContainer.data("height");
      }
      if (filtersContainer.hasClass("open")) {
        filtersContainer.css("max-height", 0);
      } else {
        filtersContainer.css("max-height", height + "px");
      }
      filtersContainer.toggleClass("open");
    });
  }
}
exports.default = gallery;

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
function accessories() {
  var accessoryModalTriggers = $("[data-open-accessory-modal]");
  if (accessoryModalTriggers.length) {
    var modal = $(".accessories-modal"),
        modalImage = $(".accessories-modal__image"),
        modalTitle = $(".accessories-modal__title"),
        modalFeatures = $(".accessories-modal__features"),
        modalDesc = $(".accessories-modal__desc"),
        body = $("body");

    accessoryModalTriggers.on("click", function () {
      var accessory = $(this).parents(".accessory"),
          accessoryTitle = accessory.find(".accessory__title"),
          accessoryImage = accessory.find(".accessory__image"),
          accessoryFeatures = accessory.find(".accessory__features"),
          accessoryDesc = accessory.find(".accessory__desc");
      modalImage.attr({
        src: accessoryImage.attr("src"),
        alt: accessoryImage.attr("alt")
      });
      modalTitle.html(accessoryTitle.html());
      modalFeatures.html(accessoryFeatures.html());
      modalDesc.html(accessoryDesc.html());
      modal.fadeIn(500, function () {
        body.addClass("no-scroll");
      });
    });
  }
}

exports.default = accessories;

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
function system() {
  var images = $(".system-page__gallery__image"),
      imagesNumber = images.length,
      modelsTables = $(".system-page__models__table");
  if (imagesNumber) {
    var controls = $(".system-page__gallery__control");
    controls.on("click", function () {
      var $this = $(this),
          activeImage = $(".system-page__gallery__image.active"),
          index = images.index(activeImage);

      if ($this.hasClass("next")) {
        if (index < imagesNumber - 1) {
          images.eq(index + 1).addClass("active");
        } else {
          images.eq(0).addClass("active");
        }
      } else {
        if (index === 0) {
          images.eq(imagesNumber - 1).addClass("active");
        } else {
          images.eq(index - 1).addClass("active");
        }
      }
      activeImage.removeClass("active");
    });
  }
  if (modelsTables.length) {
    var height = modelsTables.eq(0).height();
    modelsTables.eq(1).height(height);
  }
}

exports.default = system;

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
function forms() {
  var formGroup = $(".form-group");
  formGroup.on("click", function () {
    var input = $(this).find("input");
    input.focus();
  });
}
exports.default = forms;

/***/ })
/******/ ]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgY2MzNjYxNGQxNjM0YzNmZTQ3NTYiLCJ3ZWJwYWNrOi8vLy4vc3JjL2Fzc2V0cy9qcy9hcHAuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2Fzc2V0cy9qcy9wYXJ0aWFscy9zbGlkZXIuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2Fzc2V0cy9qcy9wYXJ0aWFscy9tb2RhbHMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2Fzc2V0cy9qcy9wYXJ0aWFscy9nYWxsZXJ5LmpzIiwid2VicGFjazovLy8uL3NyYy9hc3NldHMvanMvcGFydGlhbHMvYWNjZXNzb3JpZXMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2Fzc2V0cy9qcy9wYXJ0aWFscy9zeXN0ZW0uanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2Fzc2V0cy9qcy9wYXJ0aWFscy9mb3Jtcy5qcyJdLCJuYW1lcyI6WyJ3aW5kb3ciLCIkIiwibG9hZGluZyIsIm9wZW5NZW51IiwiY2xvc2VNZW51IiwibWVudSIsIm9uIiwiZXZlbnQiLCJwcmV2ZW50RGVmYXVsdCIsImFkZENsYXNzIiwicmVtb3ZlQ2xhc3MiLCJzbGlkZXIiLCJnZXRUcmFuc2xhdGVYIiwibXlFbGVtZW50Iiwic3R5bGUiLCJnZXRDb21wdXRlZFN0eWxlIiwibWF0cml4IiwiV2ViS2l0Q1NTTWF0cml4Iiwid2Via2l0VHJhbnNmb3JtIiwibTQxIiwibGVuZ3RoIiwiZHJhZ1N0YXJ0IiwiZSIsImNsZWFySW50ZXJ2YWwiLCJ0aW1lciIsInN0b3BQcm9wYWdhdGlvbiIsInNsaWRlc0NvbnRhaW5lciIsImN1cnJlbnRYIiwiY3NzIiwiYWN0aXZlIiwidHlwZSIsImludGlhbE1vdXNlUG9zaXRpb24iLCJ0b3VjaGVzIiwiY2xpZW50WCIsImRyYWdFbmQiLCJmaW5hbFBvc2l0aW9uIiwicGVyY2VudGFnZSIsIk1hdGgiLCJyb3VuZCIsIndpZHRoIiwiZGlyZWN0aW9uIiwidHJhbnNpdGlvbkR1cmF0aW9uIiwiZHVyYXRpb24iLCJmcmFtZSIsInNldEludGVydmFsIiwiY3VyZW50UG9zaXRpb24iLCJvZmYiLCJtb3VzZUxlYXZlIiwidHJpZ2dlciIsImRyYWciLCJkcmFnWCIsImVsZW1lbnQiLCJkb2N1bWVudCIsImVsZW1lbnRGcm9tUG9pbnQiLCJjbGllbnRZIiwiaXMiLCJkZWx0YSIsInBvc2l0aW9uIiwic2xpZGVzIiwic2Nyb2xsV2lkdGgiLCJzbGlkZXNOdW1iZXIiLCJtb2RhbHMiLCJ0cmlnZ2VycyIsImNsb3NlTW9kYWwiLCJib2R5IiwiJHRoaXMiLCJ0YXJnZXQiLCJkYXRhIiwibW9kYWwiLCJmYWRlSW4iLCJpZCIsInNvdXJjZSIsInZpZGVvIiwiZmluZCIsImF0dHIiLCJ1cmwiLCJwYXJlbnRzIiwiZmFkZU91dCIsIm1ha2VNb2RhbEltYWdlIiwiaW1hZ2UiLCJmaXJzdCIsIm5hbWUiLCJnYWxsZXJ5IiwiZ2FsbGVyeUZpbHRlcnMiLCJnb3RvU2xpZGUiLCJpbmRleCIsImFjdGl2ZUltYWdlIiwibW9kYWxJbWFnZXNUcmFjayIsImdhbGxlcnlNb2RhbEltYWdlV2lkdGgiLCJtb2RhbENhcHRpb25zIiwiaHRtbCIsImluZm8iLCJtb2RhbENvdW50RnJvbSIsIm1vZGFsSW1hZ2VzIiwiZXEiLCJjb250cm9scyIsIm1vZGFsSW1hZ2VzTnVtYmVyIiwibmV4dCIsInByZXYiLCJpbWFnZXNDb250YWluZXIiLCJnYWxsZXJ5SW1hZ2VzIiwibW9kYWxDb3VudFRvIiwiZ2FsbGVyeU1vZGFsIiwib3V0ZXJXaWR0aCIsImN1cnJlbnRGaWx0ZXIiLCJvcGVuZWRNb2RhbCIsIm1vYmlsZUZpbHRlcnNCdG4iLCJmaWx0ZXJzQ29udGFpbmVyIiwiaW1hZ2VzIiwiYWN0aXZlRmlsdGVyIiwiaGFzQ2xhc3MiLCJmaWx0ZXIiLCJnZXQiLCJ0aGVuIiwiJHJlc3AiLCJyZXNwIiwibmV3SW5mbyIsImhlaWdodCIsInNjcm9sbEhlaWdodCIsInRvZ2dsZUNsYXNzIiwiYWNjZXNzb3JpZXMiLCJhY2Nlc3NvcnlNb2RhbFRyaWdnZXJzIiwibW9kYWxJbWFnZSIsIm1vZGFsVGl0bGUiLCJtb2RhbEZlYXR1cmVzIiwibW9kYWxEZXNjIiwiYWNjZXNzb3J5IiwiYWNjZXNzb3J5VGl0bGUiLCJhY2Nlc3NvcnlJbWFnZSIsImFjY2Vzc29yeUZlYXR1cmVzIiwiYWNjZXNzb3J5RGVzYyIsInNyYyIsImFsdCIsInN5c3RlbSIsImltYWdlc051bWJlciIsIm1vZGVsc1RhYmxlcyIsImZvcm1zIiwiZm9ybUdyb3VwIiwiaW5wdXQiLCJmb2N1cyJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7QUM3REE7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7Ozs7QUFDQUEsT0FBT0MsQ0FBUCxHQUFXQSxDQUFYO0FBQ08sSUFBTUMscUVBQU47O0FBRVAsSUFBSUMsV0FBV0YsRUFBRSxtQkFBRixDQUFmO0FBQUEsSUFDRUcsWUFBWUgsRUFBRSxvQkFBRixDQURkO0FBQUEsSUFFRUksT0FBT0osRUFBRSx1QkFBRixDQUZUOztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQUUsU0FBU0csRUFBVCxDQUFZLE9BQVosRUFBcUIsVUFBU0MsS0FBVCxFQUFnQjtBQUNuQ0EsUUFBTUMsY0FBTjtBQUNBSCxPQUFLSSxRQUFMLENBQWMsS0FBZDtBQUNELENBSEQ7QUFJQUwsVUFBVUUsRUFBVixDQUFhLE9BQWIsRUFBc0IsVUFBU0MsS0FBVCxFQUFnQjtBQUNwQ0EsUUFBTUMsY0FBTjtBQUNBSCxPQUFLSyxXQUFMLENBQWlCLEtBQWpCO0FBQ0QsQ0FIRCxFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDeEJBLFNBQVNDLE1BQVQsR0FBa0I7QUFDaEIsTUFBSUEsU0FBU1YsRUFBRSxvQkFBRixDQUFiOztBQUVBLFdBQVNXLGFBQVQsQ0FBdUJDLFNBQXZCLEVBQWtDO0FBQ2hDLFFBQUlDLFFBQVFkLE9BQU9lLGdCQUFQLENBQXdCRixTQUF4QixDQUFaO0FBQUEsUUFDRUcsU0FBUyxJQUFJQyxlQUFKLENBQW9CSCxNQUFNSSxlQUExQixDQURYO0FBRUEsV0FBT0YsT0FBT0csR0FBZDtBQUNEOztBQUVELE1BQUlSLE9BQU9TLE1BQVgsRUFBbUI7QUFBQSxRQXNCUkMsU0F0QlEsR0FzQmpCLFNBQVNBLFNBQVQsQ0FBbUJDLENBQW5CLEVBQXNCO0FBQ3BCQyxvQkFBY0MsS0FBZDtBQUNBRixRQUFFZCxjQUFGO0FBQ0FjLFFBQUVHLGVBQUY7QUFDQUMsc0JBQWdCaEIsV0FBaEIsQ0FBNEIsTUFBNUI7QUFDQWdCLHNCQUFnQmhCLFdBQWhCLENBQTRCLFFBQTVCO0FBQ0FnQixzQkFBZ0JoQixXQUFoQixDQUE0QixPQUE1QjtBQUNBaUIsaUJBQVdmLGNBQWNjLGdCQUFnQixDQUFoQixDQUFkLENBQVg7QUFDQUEsc0JBQWdCRSxHQUFoQixDQUFvQixXQUFwQixrQkFBK0NELFFBQS9DO0FBQ0FELHNCQUFnQmhCLFdBQWhCLENBQTRCLFNBQTVCO0FBQ0FnQixzQkFBZ0JoQixXQUFoQixDQUE0QixjQUE1QjtBQUNBbUIsZUFBUyxJQUFUO0FBQ0EsVUFBSVAsRUFBRVEsSUFBRixLQUFXLFlBQWYsRUFBNkI7QUFDM0JDLDhCQUFzQlQsRUFBRVUsT0FBRixDQUFVLENBQVYsRUFBYUMsT0FBbkM7QUFDRCxPQUZELE1BRU87QUFDTEYsOEJBQXNCVCxFQUFFVyxPQUF4QjtBQUNEO0FBQ0YsS0F2Q2dCOztBQUFBLFFBeUNSQyxPQXpDUSxHQXlDakIsU0FBU0EsT0FBVCxDQUFpQlosQ0FBakIsRUFBb0I7QUFDbEJBLFFBQUVkLGNBQUY7QUFDQWMsUUFBRUcsZUFBRjtBQUNBLFVBQUlJLE1BQUosRUFBWTtBQUNWQSxpQkFBUyxLQUFUO0FBQ0EsWUFBSU0sZ0JBQ0F2QixjQUFjYyxnQkFBZ0IsQ0FBaEIsQ0FBZCxJQUFvQyxDQUFDLEVBQXJDLEdBQ0ksQ0FESixHQUVJZCxjQUFjYyxnQkFBZ0IsQ0FBaEIsQ0FBZCxDQUhSO0FBQUEsWUFJRVUsYUFBYSxDQUFDQyxLQUFLQyxLQUFMLENBQVlILGdCQUFnQixHQUFqQixHQUF3QkksS0FBbkMsQ0FKaEI7QUFLQSxZQUFJSixnQkFBZ0IsQ0FBcEIsRUFBdUI7QUFDckIsY0FBSUssY0FBYyxNQUFsQixFQUEwQjtBQUN4QixnQkFBSUMscUJBQ0FELGNBQWMsTUFBZCxHQUNJLENBQUMsSUFBSUosYUFBYSxHQUFsQixJQUF5Qk0sUUFEN0IsR0FFS04sYUFBYU0sUUFBZCxHQUEwQixHQUhsQztBQUFBLGdCQUlFQyxRQUNHLENBQUMsSUFBSVAsYUFBYSxHQUFsQixJQUF5QkcsS0FBMUIsSUFDRUUscUJBQXFCLElBQXRCLEdBQThCLEVBRC9CLENBTEo7QUFPRCxXQVJELE1BUU87QUFDTCxnQkFBSUEscUJBQXNCTCxhQUFhTSxRQUFkLEdBQTBCLEdBQW5EO0FBQUEsZ0JBQ0VDLFFBQ0dQLGFBQWFHLEtBQWQsR0FBdUIsR0FBdkIsSUFBK0JFLHFCQUFxQixJQUF0QixHQUE4QixFQUE1RCxDQUZKO0FBR0Q7QUFDRixTQWRELE1BY087QUFDTGYsMEJBQWdCRSxHQUFoQixDQUFvQixXQUFwQixrQkFBK0MsQ0FBL0M7QUFDRDs7QUFFRCxZQUFJWSxjQUFjLE1BQWxCLEVBQTBCO0FBQ3hCaEIsa0JBQVFvQixZQUFZLFlBQVc7QUFDN0IsZ0JBQUlDLGlCQUFpQmpDLGNBQWNjLGdCQUFnQixDQUFoQixDQUFkLENBQXJCO0FBQ0EsZ0JBQUksQ0FBQ21CLGNBQUQsSUFBbUJOLEtBQXZCLEVBQThCO0FBQzVCaEIsNEJBQWNDLEtBQWQ7QUFDQUUsOEJBQWdCakIsUUFBaEIsQ0FBeUIsY0FBekI7QUFDQWlCLDhCQUFnQmpCLFFBQWhCLENBQXlCLE1BQXpCO0FBQ0E7QUFDRCxhQUxELE1BS087QUFDTGlCLDhCQUFnQkUsR0FBaEIsQ0FDRSxXQURGLG1CQUVnQmlCLGlCQUFpQkYsS0FGakM7QUFJRDtBQUNGLFdBYk8sRUFhTCxFQWJLLENBQVI7QUFjRCxTQWZELE1BZU87QUFDTG5CLGtCQUFRb0IsWUFBWSxZQUFXO0FBQzdCLGdCQUFJQyxpQkFBaUJqQyxjQUFjYyxnQkFBZ0IsQ0FBaEIsQ0FBZCxDQUFyQjtBQUNBLGdCQUFJbUIsa0JBQWtCLENBQXRCLEVBQXlCO0FBQ3ZCdEIsNEJBQWNDLEtBQWQ7QUFDQUUsOEJBQWdCakIsUUFBaEIsQ0FBeUIsU0FBekI7QUFDQWlCLDhCQUFnQmpCLFFBQWhCLENBQXlCLE1BQXpCO0FBQ0E7QUFDRCxhQUxELE1BS087QUFDTGlCLDhCQUFnQkUsR0FBaEIsQ0FDRSxXQURGLG1CQUVnQmUsUUFBUUUsY0FGeEI7QUFJRDtBQUNGLFdBYk8sRUFhTCxFQWJLLENBQVI7QUFjRDtBQUNEbkIsd0JBQWdCb0IsR0FBaEIsQ0FBb0IsZUFBcEI7QUFDQXBCLHdCQUFnQnBCLEVBQWhCLENBQW1CLGVBQW5CLEVBQW9DLFlBQVc7QUFDN0NvQiwwQkFBZ0JoQixXQUFoQixDQUE0QixRQUE1QjtBQUNBZ0IsMEJBQWdCaEIsV0FBaEIsQ0FBNEIsT0FBNUI7QUFDQWdCLDBCQUFnQkUsR0FBaEIsQ0FBb0IscUJBQXBCLEVBQThDLENBQTlDO0FBQ0EsY0FBSVksY0FBYyxPQUFsQixFQUEyQjtBQUN6QmQsNEJBQWdCakIsUUFBaEIsQ0FBeUIsU0FBekI7QUFDRCxXQUZELE1BRU87QUFDTGlCLDRCQUFnQmpCLFFBQWhCLENBQXlCLGNBQXpCO0FBQ0Q7QUFDRGlCLDBCQUFnQmpCLFFBQWhCLENBQXlCLE1BQXpCO0FBQ0QsU0FWRDtBQVdEO0FBQ0YsS0FqSGdCOztBQUFBLFFBa0hSc0MsVUFsSFEsR0FrSGpCLFNBQVNBLFVBQVQsQ0FBb0J6QixDQUFwQixFQUF1QjtBQUNyQkEsUUFBRWQsY0FBRjtBQUNBYyxRQUFFRyxlQUFGO0FBQ0EsVUFBSUksTUFBSixFQUFZO0FBQ1ZILHdCQUFnQnNCLE9BQWhCLENBQXdCLFNBQXhCO0FBQ0Q7QUFDRixLQXhIZ0I7O0FBQUEsUUEwSFJDLElBMUhRLEdBMEhqQixTQUFTQSxJQUFULENBQWMzQixDQUFkLEVBQWlCO0FBQ2ZBLFFBQUVkLGNBQUY7QUFDQWMsUUFBRUcsZUFBRjtBQUNBLFVBQUl5QixjQUFKO0FBQ0EsVUFBSXJCLE1BQUosRUFBWTtBQUNWLFlBQUlQLEVBQUVRLElBQUYsS0FBVyxXQUFmLEVBQTRCO0FBQzFCUixZQUFFZCxjQUFGO0FBQ0EsY0FBSTJDLFVBQVVsRCxFQUNabUQsU0FBU0MsZ0JBQVQsQ0FDRS9CLEVBQUVVLE9BQUYsQ0FBVSxDQUFWLEVBQWFDLE9BRGYsRUFFRVgsRUFBRVUsT0FBRixDQUFVLENBQVYsRUFBYXNCLE9BRmYsQ0FEWSxDQUFkO0FBTUEsY0FBSSxDQUFDSCxRQUFRSSxFQUFSLENBQVc3QixlQUFYLENBQUwsRUFBa0M7QUFDaENBLDRCQUFnQnNCLE9BQWhCLENBQXdCLFVBQXhCO0FBQ0Q7QUFDREUsa0JBQVE1QixFQUFFVSxPQUFGLENBQVUsQ0FBVixFQUFhQyxPQUFyQjtBQUNELFNBWkQsTUFZTztBQUNMaUIsa0JBQVE1QixFQUFFVyxPQUFWO0FBQ0Q7QUFDRCxZQUFJdUIsUUFBUSxDQUFDTixLQUFELEdBQVNuQixtQkFBckI7QUFBQSxZQUNFMEIsV0FBVzlCLFdBQVc2QixLQUR4QjtBQUVBLFlBQUlOLFFBQVFuQixtQkFBWixFQUFpQztBQUMvQlMsc0JBQVksT0FBWjtBQUNELFNBRkQsTUFFTztBQUNMQSxzQkFBWSxNQUFaO0FBQ0Q7QUFDRCxZQUFJaUIsV0FBVyxDQUFYLElBQWdCQSxXQUFXLENBQUNsQixLQUFoQyxFQUNFYixnQkFBZ0JFLEdBQWhCLENBQW9CLFdBQXBCLGtCQUErQzZCLFFBQS9DO0FBQ0gsT0F6QkQsTUF5Qk87QUFDTHZCLGdCQUFRO0FBQ05ELG1CQUFTLENBREg7QUFFTnpCLHdCQUZNLDRCQUVXLENBQUUsQ0FGYjtBQUdOaUIseUJBSE0sNkJBR1ksQ0FBRTtBQUhkLFNBQVI7QUFLRDtBQUNGLEtBOUpnQjs7QUFDakIsUUFBSWlDLFNBQVN6RCxFQUFFLDJCQUFGLENBQWI7QUFBQSxRQUNFeUIsa0JBQWtCekIsRUFBRSxxQ0FBRixDQURwQjtBQUFBLFFBRUVzQyxRQUFRYixnQkFBZ0IsQ0FBaEIsRUFBbUJpQyxXQUY3QjtBQUFBLFFBR0VDLGVBQWVGLE9BQU90QyxNQUh4QjtBQUFBLFFBSUVzQixXQUFXa0IsZUFBZSxDQUo1QjtBQUtBbEMsb0JBQWdCRSxHQUFoQixDQUFvQixvQkFBcEIsRUFBNkNjLFFBQTdDO0FBQ0FoQixvQkFBZ0JFLEdBQWhCLENBQW9CLE9BQXBCLEVBQWdDVyxLQUFoQztBQUNBYixvQkFBZ0JqQixRQUFoQixDQUF5QixTQUF6QjtBQUNBaUIsb0JBQWdCakIsUUFBaEIsQ0FBeUIsTUFBekI7O0FBRUEsUUFBSWtCLGlCQUFKO0FBQUEsUUFBY0UsZUFBZDtBQUFBLFFBQXNCRSw0QkFBdEI7QUFBQSxRQUEyQ1Msa0JBQTNDO0FBQUEsUUFBc0RoQixjQUF0RDs7QUFFQUUsb0JBQWdCcEIsRUFBaEIsQ0FBbUIsWUFBbkIsRUFBaUNlLFNBQWpDO0FBQ0FLLG9CQUFnQnBCLEVBQWhCLENBQW1CLFVBQW5CLEVBQStCNEIsT0FBL0I7QUFDQVIsb0JBQWdCcEIsRUFBaEIsQ0FBbUIsV0FBbkIsRUFBZ0MyQyxJQUFoQzs7QUFFQXZCLG9CQUFnQnBCLEVBQWhCLENBQW1CLFdBQW5CLEVBQWdDZSxTQUFoQztBQUNBSyxvQkFBZ0JwQixFQUFoQixDQUFtQixTQUFuQixFQUE4QjRCLE9BQTlCO0FBQ0FSLG9CQUFnQnBCLEVBQWhCLENBQW1CLFlBQW5CLEVBQWlDeUMsVUFBakM7QUFDQXJCLG9CQUFnQnBCLEVBQWhCLENBQW1CLFdBQW5CLEVBQWdDMkMsSUFBaEM7QUEySUQ7QUFDRjtrQkFDY3RDLE07Ozs7Ozs7Ozs7OztBQzFLZixTQUFTa0QsTUFBVCxHQUFrQjtBQUNoQixNQUFJQyxXQUFXN0QsRUFBRSxtQkFBRixDQUFmO0FBQUEsTUFDRThELGFBQWE5RCxFQUFFLG9CQUFGLENBRGY7QUFBQSxNQUVFK0QsT0FBTy9ELEVBQUUsTUFBRixDQUZUO0FBR0E2RCxXQUFTeEQsRUFBVCxDQUFZLE9BQVosRUFBcUIsWUFBVztBQUM5QixRQUFJMkQsUUFBUWhFLEVBQUUsSUFBRixDQUFaO0FBQUEsUUFDRWlFLFNBQVNELE1BQU1FLElBQU4sQ0FBVyxPQUFYLENBRFg7QUFBQSxRQUVFQyxRQUFRbkUsaUNBQThCaUUsTUFBOUIsU0FGVjs7QUFJQUUsVUFBTUMsTUFBTixDQUFhLE1BQWIsRUFBcUIsWUFBVztBQUM5QixVQUFJSCxXQUFXLE9BQWYsRUFBd0I7QUFDdEIsWUFBSUksS0FBS0wsTUFBTUUsSUFBTixDQUFXLElBQVgsQ0FBVDtBQUFBLFlBQ0VJLFNBQVNOLE1BQU1FLElBQU4sQ0FBVyxRQUFYLENBRFg7QUFBQSxZQUVFSyxRQUFRSixNQUFNSyxJQUFOLENBQVcscUJBQVgsQ0FGVjtBQUdBLFlBQUksQ0FBQ0QsTUFBTUUsSUFBTixDQUFXLEtBQVgsQ0FBTCxFQUF3QjtBQUN0QixjQUFJQyxNQUFNLEVBQVY7QUFDQSxjQUFJSixXQUFXLFNBQWYsRUFBMEI7QUFDeEJJLHFEQUF1Q0wsRUFBdkM7QUFDRCxXQUZELE1BRU8sSUFBSUMsV0FBVyxRQUFmLEVBQXlCO0FBQzlCSSxzREFBd0NMLEVBQXhDO0FBQ0QsV0FGTSxNQUVBLElBQUlDLFdBQVcsT0FBZixFQUF3QjtBQUM3Qkksc0RBQXdDTCxFQUF4QztBQUNEO0FBQ0RFLGdCQUFNRSxJQUFOLENBQVcsS0FBWCxFQUFrQkMsR0FBbEI7QUFDRDtBQUNEO0FBQ0Q7QUFDRixLQWxCRDtBQW1CRCxHQXhCRDtBQXlCQVosYUFBV3pELEVBQVgsQ0FBYyxPQUFkLEVBQXVCLFlBQVc7QUFDaEMsUUFBSTJELFFBQVFoRSxFQUFFLElBQUYsQ0FBWjtBQUFBLFFBQ0VtRSxRQUFRSCxNQUFNVyxPQUFOLGdCQURWO0FBQUEsUUFFRTlDLE9BQU9zQyxNQUFNRCxJQUFOLENBQVcsT0FBWCxDQUZUO0FBR0EsUUFBSXJDLFNBQVMsT0FBYixFQUFzQjtBQUNwQnNDLFlBQU1LLElBQU4sQ0FBVyxxQkFBWCxFQUFrQ0MsSUFBbEMsQ0FBdUMsS0FBdkMsRUFBOEMsRUFBOUM7QUFDRDtBQUNETixVQUFNUyxPQUFOLENBQWMsTUFBZCxFQUFzQixZQUFXO0FBQy9CYixXQUFLdEQsV0FBTCxDQUFpQixXQUFqQjtBQUNELEtBRkQ7QUFHRCxHQVZEO0FBV0Q7a0JBQ2NtRCxNOzs7Ozs7Ozs7Ozs7O0FDekNmOztBQUVBLFNBQVNpQixjQUFULENBQXdCQyxLQUF4QixFQUErQkMsS0FBL0IsRUFBc0M7QUFDcEMsdURBQWlEQSxRQUFRLFFBQVIsR0FBbUIsRUFBcEUsNkJBQ2NELE1BQU1KLEdBRHBCLGlCQUNpQ0ksTUFBTUUsSUFEdkM7QUFFRDs7QUFFRCxTQUFTQyxPQUFULEdBQW1CO0FBQ2pCLE1BQUlDLGlCQUFpQmxGLEVBQUUsdUJBQUYsQ0FBckI7QUFDQSxNQUFJa0YsZUFBZS9ELE1BQW5CLEVBQTJCO0FBQUEsUUFtQmhCZ0UsU0FuQmdCLEdBbUJ6QixTQUFTQSxTQUFULENBQW1CQyxLQUFuQixFQUEwQjtBQUN4QixVQUFJQyxjQUFjckYsRUFBRSxvQ0FBRixDQUFsQjtBQUNBcUYsa0JBQVk1RSxXQUFaLENBQXdCLFFBQXhCO0FBQ0E2RSx1QkFBaUIzRCxHQUFqQixDQUNFLFdBREYsa0JBRWdCLENBQUN5RCxLQUFELEdBQVNHLHNCQUZ6QjtBQUlBQyxvQkFBY0MsSUFBZCxDQUFtQkMsS0FBS04sS0FBTCxFQUFZSixJQUEvQjtBQUNBVyxxQkFBZUYsSUFBZixDQUFvQkwsUUFBUSxDQUE1QjtBQUNBUSxrQkFBWUMsRUFBWixDQUFlVCxLQUFmLEVBQXNCNUUsUUFBdEIsQ0FBK0IsUUFBL0I7O0FBRUFzRixlQUFTckYsV0FBVCxDQUFxQixVQUFyQjtBQUNBLFVBQUkyRSxRQUFRLENBQVIsS0FBY1csaUJBQWxCLEVBQXFDO0FBQ25DQyxhQUFLeEYsUUFBTCxDQUFjLFVBQWQ7QUFDRDtBQUNELFVBQUk0RSxVQUFVLENBQWQsRUFBaUI7QUFDZmEsYUFBS3pGLFFBQUwsQ0FBYyxVQUFkO0FBQ0Q7QUFDRixLQXJDd0I7O0FBQ3pCLFFBQUkwRixrQkFBa0JsRyxFQUFFLHVCQUFGLENBQXRCO0FBQUEsUUFDRW1HLGdCQUFnQm5HLEVBQUUsc0JBQUYsQ0FEbEI7QUFBQSxRQUVFc0YsbUJBQW1CdEYsRUFBRSxvQ0FBRixDQUZyQjtBQUFBLFFBR0V3RixnQkFBZ0J4RixFQUFFLCtCQUFGLENBSGxCO0FBQUEsUUFJRTJGLGlCQUFpQjNGLEVBQUUsc0NBQUYsQ0FKbkI7QUFBQSxRQUtFb0csZUFBZXBHLEVBQUUsb0NBQUYsQ0FMakI7QUFBQSxRQU1FOEYsV0FBVzlGLEVBQUUsK0JBQUYsQ0FOYjtBQUFBLFFBT0VpRyxPQUFPakcsRUFBRSxvQ0FBRixDQVBUO0FBQUEsUUFRRWdHLE9BQU9oRyxFQUFFLG9DQUFGLENBUlQ7QUFBQSxRQVNFNEYsY0FBYzVGLEVBQUUsNkJBQUYsQ0FUaEI7QUFBQSxRQVVFK0Ysb0JBQW9CSCxZQUFZekUsTUFWbEM7QUFBQSxRQVdFa0YsZUFBZXJHLEVBQUUsY0FBRixDQVhqQjtBQUFBLFFBWUV1Rix5QkFBeUJ2RixFQUFFLDZCQUFGLEVBQWlDc0csVUFBakMsRUFaM0I7QUFBQSxRQWFFWixPQUFPUSxnQkFBZ0JoQyxJQUFoQixDQUFxQixNQUFyQixDQWJUO0FBQUEsUUFjRXFDLGdCQUFnQixFQWRsQjtBQUFBLFFBZUVDLGNBQWMsS0FmaEI7QUFBQSxRQWdCRUMsbUJBQW1CekcsRUFBRSxtQ0FBRixDQWhCckI7QUFBQSxRQWlCRTBHLG1CQUFtQjFHLEVBQUUsaUNBQUYsQ0FqQnJCOzs7QUFzQ0FrRyxvQkFBZ0I3RixFQUFoQixDQUFtQixPQUFuQixFQUE0QixzQkFBNUIsRUFBb0QsWUFBVztBQUM3RCxVQUFJMkQsUUFBUWhFLEVBQUUsSUFBRixDQUFaO0FBQUEsVUFDRW9GLFFBQVFlLGNBQWNmLEtBQWQsQ0FBb0JwQixLQUFwQixDQURWO0FBRUEsVUFBSSxDQUFDd0MsV0FBTCxFQUFrQjtBQUNoQixZQUFJRyxTQUFTLEVBQWI7QUFDQSxhQUFLLElBQUk3QixLQUFULElBQWtCWSxJQUFsQixFQUF3QjtBQUN0QmlCLG9CQUFVOUIsZUFBZWEsS0FBS1osS0FBTCxDQUFmLEVBQTRCQSxVQUFVLENBQVYsR0FBYyxJQUFkLEdBQXFCLEtBQWpELENBQVY7QUFDRDtBQUNEUSx5QkFBaUJHLElBQWpCLENBQXNCa0IsTUFBdEI7QUFDQWYsc0JBQWM1RixFQUFFLDZCQUFGLENBQWQ7QUFDQStGLDRCQUFvQkgsWUFBWXpFLE1BQWhDO0FBQ0F3RSx1QkFBZUYsSUFBZixDQUFvQkwsUUFBUSxDQUE1QjtBQUNBZ0IscUJBQWFYLElBQWIsQ0FBa0JNLGlCQUFsQjtBQUNBUyxzQkFBYyxJQUFkO0FBQ0Q7QUFDRHJCLGdCQUFVQyxLQUFWO0FBQ0FpQixtQkFBYWpDLE1BQWIsQ0FBb0IsR0FBcEI7QUFDRCxLQWpCRDs7QUFtQkFjLG1CQUFlN0UsRUFBZixDQUFrQixPQUFsQixFQUEyQixZQUFXO0FBQ3BDLFVBQUkyRCxRQUFRaEUsRUFBRSxJQUFGLENBQVo7QUFBQSxVQUNFNEcsZUFBZTVHLEVBQUUsOEJBQUYsQ0FEakI7QUFFQSxVQUFJLENBQUNnRSxNQUFNNkMsUUFBTixDQUFlLFFBQWYsQ0FBTCxFQUErQjtBQUM3QixZQUFJQyxTQUFTOUMsTUFBTUUsSUFBTixDQUFXLEtBQVgsQ0FBYjtBQUNBZ0Msd0JBQWdCVCxJQUFoQixDQUFxQnhGLFlBQXJCO0FBQ0FELFVBQUUrRyxHQUFGLHVCQUEwQkQsTUFBMUIsRUFBb0NFLElBQXBDLENBQXlDLGdCQUFRO0FBQy9DLGNBQUlDLFFBQVFqSCxFQUFFa0gsSUFBRixDQUFaO0FBQUEsY0FDRUMsVUFBVUYsTUFBTXpDLElBQU4sQ0FBVyx1QkFBWCxFQUFvQ04sSUFBcEMsQ0FBeUMsTUFBekMsQ0FEWjtBQUVBd0IsaUJBQU95QixPQUFQO0FBQ0FaLDBCQUFnQk8sTUFBaEI7QUFDQVosMEJBQWdCVCxJQUFoQixDQUFxQndCLE1BQU16QyxJQUFOLENBQVcsc0JBQVgsQ0FBckI7QUFDQTBCLDBCQUFnQmhDLElBQWhCLENBQXFCLE1BQXJCLEVBQTZCaUQsT0FBN0I7QUFDQVAsdUJBQWFuRyxXQUFiLENBQXlCLFFBQXpCO0FBQ0F1RCxnQkFBTXhELFFBQU4sQ0FBZSxRQUFmO0FBQ0FnRyx3QkFBYyxLQUFkO0FBQ0FMLDBCQUFnQm5HLEVBQUUsc0JBQUYsQ0FBaEI7QUFDRCxTQVhEO0FBWUQ7QUFDRixLQW5CRDs7QUFxQkE4RixhQUFTekYsRUFBVCxDQUFZLE9BQVosRUFBcUIsWUFBVztBQUM5QixVQUFJMkQsUUFBUWhFLEVBQUUsSUFBRixDQUFaO0FBQUEsVUFDRW9GLFFBQVFRLFlBQVlSLEtBQVosQ0FBa0JwRixFQUFFLG9DQUFGLENBQWxCLENBRFY7QUFFQSxVQUFJZ0UsTUFBTTZDLFFBQU4sQ0FBZSxNQUFmLENBQUosRUFBNEI7QUFDMUIsWUFBSXpCLFFBQVFXLG9CQUFvQixDQUFoQyxFQUFtQztBQUNqQ1osb0JBQVVDLFFBQVEsQ0FBbEI7QUFDQUEsa0JBQVEsQ0FBUixLQUFjVyxvQkFBb0IsQ0FBbEMsR0FDSS9CLE1BQU14RCxRQUFOLENBQWUsVUFBZixDQURKLEdBRUlzRixTQUFTckYsV0FBVCxDQUFxQixVQUFyQixDQUZKO0FBR0Q7QUFDRixPQVBELE1BT08sSUFBSXVELE1BQU02QyxRQUFOLENBQWUsTUFBZixDQUFKLEVBQTRCO0FBQ2pDLFlBQUl6QixRQUFRLENBQVosRUFBZTtBQUNiRCxvQkFBVUMsUUFBUSxDQUFsQjtBQUNEO0FBQ0Y7QUFDRixLQWZEO0FBZ0JBcUIscUJBQWlCcEcsRUFBakIsQ0FBb0IsT0FBcEIsRUFBNkIsVUFBU0MsS0FBVCxFQUFnQjtBQUMzQ0EsWUFBTUMsY0FBTjtBQUNBLFVBQUl5RCxRQUFRaEUsRUFBRSxJQUFGLENBQVo7QUFBQSxVQUNFb0gsZUFERjtBQUVBLFVBQUksQ0FBQ1YsaUJBQWlCeEMsSUFBakIsQ0FBc0IsUUFBdEIsQ0FBTCxFQUFzQztBQUNwQ2tELGlCQUFTVixpQkFBaUIsQ0FBakIsRUFBb0JXLFlBQTdCO0FBQ0FYLHlCQUFpQnhDLElBQWpCLENBQXNCLFFBQXRCLEVBQWdDa0QsTUFBaEM7QUFDRCxPQUhELE1BR087QUFDTEEsaUJBQVNWLGlCQUFpQnhDLElBQWpCLENBQXNCLFFBQXRCLENBQVQ7QUFDRDtBQUNELFVBQUl3QyxpQkFBaUJHLFFBQWpCLENBQTBCLE1BQTFCLENBQUosRUFBdUM7QUFDckNILHlCQUFpQi9FLEdBQWpCLENBQXFCLFlBQXJCLEVBQW1DLENBQW5DO0FBQ0QsT0FGRCxNQUVPO0FBQ0wrRSx5QkFBaUIvRSxHQUFqQixDQUFxQixZQUFyQixFQUFzQ3lGLE1BQXRDO0FBQ0Q7QUFDRFYsdUJBQWlCWSxXQUFqQixDQUE2QixNQUE3QjtBQUNELEtBaEJEO0FBaUJEO0FBQ0Y7a0JBQ2NyQyxPOzs7Ozs7Ozs7Ozs7QUMzSGYsU0FBU3NDLFdBQVQsR0FBdUI7QUFDckIsTUFBSUMseUJBQXlCeEgsRUFBRSw2QkFBRixDQUE3QjtBQUNBLE1BQUl3SCx1QkFBdUJyRyxNQUEzQixFQUFtQztBQUNqQyxRQUFJZ0QsUUFBUW5FLEVBQUUsb0JBQUYsQ0FBWjtBQUFBLFFBQ0V5SCxhQUFhekgsRUFBRSwyQkFBRixDQURmO0FBQUEsUUFFRTBILGFBQWExSCxFQUFFLDJCQUFGLENBRmY7QUFBQSxRQUdFMkgsZ0JBQWdCM0gsRUFBRSw4QkFBRixDQUhsQjtBQUFBLFFBSUU0SCxZQUFZNUgsRUFBRSwwQkFBRixDQUpkO0FBQUEsUUFLRStELE9BQU8vRCxFQUFFLE1BQUYsQ0FMVDs7QUFPQXdILDJCQUF1Qm5ILEVBQXZCLENBQTBCLE9BQTFCLEVBQW1DLFlBQVc7QUFDNUMsVUFBSXdILFlBQVk3SCxFQUFFLElBQUYsRUFBUTJFLE9BQVIsQ0FBZ0IsWUFBaEIsQ0FBaEI7QUFBQSxVQUNFbUQsaUJBQWlCRCxVQUFVckQsSUFBVixDQUFlLG1CQUFmLENBRG5CO0FBQUEsVUFFRXVELGlCQUFpQkYsVUFBVXJELElBQVYsQ0FBZSxtQkFBZixDQUZuQjtBQUFBLFVBR0V3RCxvQkFBb0JILFVBQVVyRCxJQUFWLENBQWUsc0JBQWYsQ0FIdEI7QUFBQSxVQUlFeUQsZ0JBQWdCSixVQUFVckQsSUFBVixDQUFlLGtCQUFmLENBSmxCO0FBS0FpRCxpQkFBV2hELElBQVgsQ0FBZ0I7QUFDZHlELGFBQUtILGVBQWV0RCxJQUFmLENBQW9CLEtBQXBCLENBRFM7QUFFZDBELGFBQUtKLGVBQWV0RCxJQUFmLENBQW9CLEtBQXBCO0FBRlMsT0FBaEI7QUFJQWlELGlCQUFXakMsSUFBWCxDQUFnQnFDLGVBQWVyQyxJQUFmLEVBQWhCO0FBQ0FrQyxvQkFBY2xDLElBQWQsQ0FBbUJ1QyxrQkFBa0J2QyxJQUFsQixFQUFuQjtBQUNBbUMsZ0JBQVVuQyxJQUFWLENBQWV3QyxjQUFjeEMsSUFBZCxFQUFmO0FBQ0F0QixZQUFNQyxNQUFOLENBQWEsR0FBYixFQUFrQixZQUFNO0FBQ3RCTCxhQUFLdkQsUUFBTCxDQUFjLFdBQWQ7QUFDRCxPQUZEO0FBR0QsS0FoQkQ7QUFpQkQ7QUFDRjs7a0JBRWMrRyxXOzs7Ozs7Ozs7Ozs7QUM5QmYsU0FBU2EsTUFBVCxHQUFrQjtBQUNoQixNQUFJekIsU0FBUzNHLEVBQUUsOEJBQUYsQ0FBYjtBQUFBLE1BQ0VxSSxlQUFlMUIsT0FBT3hGLE1BRHhCO0FBQUEsTUFFRW1ILGVBQWV0SSxFQUFFLDZCQUFGLENBRmpCO0FBR0EsTUFBSXFJLFlBQUosRUFBa0I7QUFDaEIsUUFBSXZDLFdBQVc5RixFQUFFLGdDQUFGLENBQWY7QUFDQThGLGFBQVN6RixFQUFULENBQVksT0FBWixFQUFxQixZQUFXO0FBQzlCLFVBQUkyRCxRQUFRaEUsRUFBRSxJQUFGLENBQVo7QUFBQSxVQUNFcUYsY0FBY3JGLEVBQUUscUNBQUYsQ0FEaEI7QUFBQSxVQUVFb0YsUUFBUXVCLE9BQU92QixLQUFQLENBQWFDLFdBQWIsQ0FGVjs7QUFJQSxVQUFJckIsTUFBTTZDLFFBQU4sQ0FBZSxNQUFmLENBQUosRUFBNEI7QUFDMUIsWUFBSXpCLFFBQVFpRCxlQUFlLENBQTNCLEVBQThCO0FBQzVCMUIsaUJBQU9kLEVBQVAsQ0FBVVQsUUFBUSxDQUFsQixFQUFxQjVFLFFBQXJCLENBQThCLFFBQTlCO0FBQ0QsU0FGRCxNQUVPO0FBQ0xtRyxpQkFBT2QsRUFBUCxDQUFVLENBQVYsRUFBYXJGLFFBQWIsQ0FBc0IsUUFBdEI7QUFDRDtBQUNGLE9BTkQsTUFNTztBQUNMLFlBQUk0RSxVQUFVLENBQWQsRUFBaUI7QUFDZnVCLGlCQUFPZCxFQUFQLENBQVV3QyxlQUFlLENBQXpCLEVBQTRCN0gsUUFBNUIsQ0FBcUMsUUFBckM7QUFDRCxTQUZELE1BRU87QUFDTG1HLGlCQUFPZCxFQUFQLENBQVVULFFBQVEsQ0FBbEIsRUFBcUI1RSxRQUFyQixDQUE4QixRQUE5QjtBQUNEO0FBQ0Y7QUFDRDZFLGtCQUFZNUUsV0FBWixDQUF3QixRQUF4QjtBQUNELEtBbkJEO0FBb0JEO0FBQ0QsTUFBSTZILGFBQWFuSCxNQUFqQixFQUF5QjtBQUN2QixRQUFJaUcsU0FBU2tCLGFBQWF6QyxFQUFiLENBQWdCLENBQWhCLEVBQW1CdUIsTUFBbkIsRUFBYjtBQUNBa0IsaUJBQWF6QyxFQUFiLENBQWdCLENBQWhCLEVBQW1CdUIsTUFBbkIsQ0FBMEJBLE1BQTFCO0FBQ0Q7QUFDRjs7a0JBRWNnQixNOzs7Ozs7Ozs7Ozs7QUNqQ2YsU0FBU0csS0FBVCxHQUFpQjtBQUNmLE1BQUlDLFlBQVl4SSxFQUFFLGFBQUYsQ0FBaEI7QUFDQXdJLFlBQVVuSSxFQUFWLENBQWEsT0FBYixFQUFzQixZQUFXO0FBQy9CLFFBQUlvSSxRQUFRekksRUFBRSxJQUFGLEVBQVF3RSxJQUFSLENBQWEsT0FBYixDQUFaO0FBQ0FpRSxVQUFNQyxLQUFOO0FBQ0QsR0FIRDtBQUlEO2tCQUNjSCxLIiwiZmlsZSI6ImFwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDEpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIGNjMzY2MTRkMTYzNGMzZmU0NzU2IiwiaW1wb3J0IHNsaWRlciBmcm9tIFwiLi9wYXJ0aWFscy9zbGlkZXJcIjtcbmltcG9ydCBtb2RhbHMgZnJvbSBcIi4vcGFydGlhbHMvbW9kYWxzXCI7XG5pbXBvcnQgZ2FsbGVyeSBmcm9tIFwiLi9wYXJ0aWFscy9nYWxsZXJ5XCI7XG5pbXBvcnQgYWNjZXNzb3JpZXMgZnJvbSBcIi4vcGFydGlhbHMvYWNjZXNzb3JpZXNcIjtcbmltcG9ydCBzeXN0ZW0gZnJvbSBcIi4vcGFydGlhbHMvc3lzdGVtXCI7XG5pbXBvcnQgZm9ybXMgZnJvbSBcIi4vcGFydGlhbHMvZm9ybXNcIjtcbndpbmRvdy4kID0gJDtcbmV4cG9ydCBjb25zdCBsb2FkaW5nID0gYDxkaXYgY2xhc3M9XCJsb2FkaW5nXCI+TG9hZGluZy4uLjwvZGl2PmA7XG5cbmxldCBvcGVuTWVudSA9ICQoXCIub3Blbi1tb2JpbGUtbWVudVwiKSxcbiAgY2xvc2VNZW51ID0gJChcIi5jbG9zZS1tb2JpbGUtbWVudVwiKSxcbiAgbWVudSA9ICQoXCIuc2l0ZS1oZWFkZXJfX2FjdGlvbnNcIik7XG5cbnNsaWRlcigpO1xubW9kYWxzKCk7XG5nYWxsZXJ5KCk7XG5hY2Nlc3NvcmllcygpO1xuZm9ybXMoKTtcbnN5c3RlbSgpO1xuXG5vcGVuTWVudS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKGV2ZW50KSB7XG4gIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gIG1lbnUuYWRkQ2xhc3MoXCJvdXRcIik7XG59KTtcbmNsb3NlTWVudS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKGV2ZW50KSB7XG4gIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gIG1lbnUucmVtb3ZlQ2xhc3MoXCJvdXRcIik7XG59KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9hc3NldHMvanMvYXBwLmpzIiwiZnVuY3Rpb24gc2xpZGVyKCkge1xuICBsZXQgc2xpZGVyID0gJChcIi5jdXN0b21lcnNfX3NsaWRlclwiKTtcblxuICBmdW5jdGlvbiBnZXRUcmFuc2xhdGVYKG15RWxlbWVudCkge1xuICAgIGxldCBzdHlsZSA9IHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKG15RWxlbWVudCksXG4gICAgICBtYXRyaXggPSBuZXcgV2ViS2l0Q1NTTWF0cml4KHN0eWxlLndlYmtpdFRyYW5zZm9ybSk7XG4gICAgcmV0dXJuIG1hdHJpeC5tNDE7XG4gIH1cblxuICBpZiAoc2xpZGVyLmxlbmd0aCkge1xuICAgIGxldCBzbGlkZXMgPSAkKFwiLmN1c3RvbWVyc19fc2xpZGVyX19zbGlkZVwiKSxcbiAgICAgIHNsaWRlc0NvbnRhaW5lciA9ICQoXCIuY3VzdG9tZXJzX19zbGlkZXJfX3NsaWRlLWNvbnRhaW5lclwiKSxcbiAgICAgIHdpZHRoID0gc2xpZGVzQ29udGFpbmVyWzBdLnNjcm9sbFdpZHRoLFxuICAgICAgc2xpZGVzTnVtYmVyID0gc2xpZGVzLmxlbmd0aCxcbiAgICAgIGR1cmF0aW9uID0gc2xpZGVzTnVtYmVyICogNTtcbiAgICBzbGlkZXNDb250YWluZXIuY3NzKFwiYW5pbWF0aW9uLWR1cmF0aW9uXCIsIGAke2R1cmF0aW9ufXNgKTtcbiAgICBzbGlkZXNDb250YWluZXIuY3NzKFwid2lkdGhcIiwgYCR7d2lkdGh9cHhgKTtcbiAgICBzbGlkZXNDb250YWluZXIuYWRkQ2xhc3MoXCJtYXJxdWVlXCIpO1xuICAgIHNsaWRlc0NvbnRhaW5lci5hZGRDbGFzcyhcInBsYXlcIik7XG5cbiAgICBsZXQgY3VycmVudFgsIGFjdGl2ZSwgaW50aWFsTW91c2VQb3NpdGlvbiwgZGlyZWN0aW9uLCB0aW1lcjtcblxuICAgIHNsaWRlc0NvbnRhaW5lci5vbihcInRvdWNoc3RhcnRcIiwgZHJhZ1N0YXJ0KTtcbiAgICBzbGlkZXNDb250YWluZXIub24oXCJ0b3VjaGVuZFwiLCBkcmFnRW5kKTtcbiAgICBzbGlkZXNDb250YWluZXIub24oXCJ0b3VjaG1vdmVcIiwgZHJhZyk7XG5cbiAgICBzbGlkZXNDb250YWluZXIub24oXCJtb3VzZWRvd25cIiwgZHJhZ1N0YXJ0KTtcbiAgICBzbGlkZXNDb250YWluZXIub24oXCJtb3VzZXVwXCIsIGRyYWdFbmQpO1xuICAgIHNsaWRlc0NvbnRhaW5lci5vbihcIm1vdXNlbGVhdmVcIiwgbW91c2VMZWF2ZSk7XG4gICAgc2xpZGVzQ29udGFpbmVyLm9uKFwibW91c2Vtb3ZlXCIsIGRyYWcpO1xuXG4gICAgZnVuY3Rpb24gZHJhZ1N0YXJ0KGUpIHtcbiAgICAgIGNsZWFySW50ZXJ2YWwodGltZXIpO1xuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgIHNsaWRlc0NvbnRhaW5lci5yZW1vdmVDbGFzcyhcInBsYXlcIik7XG4gICAgICBzbGlkZXNDb250YWluZXIucmVtb3ZlQ2xhc3MoXCJmaW5pc2hcIik7XG4gICAgICBzbGlkZXNDb250YWluZXIucmVtb3ZlQ2xhc3MoXCJzdGFydFwiKTtcbiAgICAgIGN1cnJlbnRYID0gZ2V0VHJhbnNsYXRlWChzbGlkZXNDb250YWluZXJbMF0pO1xuICAgICAgc2xpZGVzQ29udGFpbmVyLmNzcyhcInRyYW5zZm9ybVwiLCBgdHJhbnNsYXRlWCgke2N1cnJlbnRYfXB4KWApO1xuICAgICAgc2xpZGVzQ29udGFpbmVyLnJlbW92ZUNsYXNzKFwibWFycXVlZVwiKTtcbiAgICAgIHNsaWRlc0NvbnRhaW5lci5yZW1vdmVDbGFzcyhcIm1hcnF1ZWUtYmFja1wiKTtcbiAgICAgIGFjdGl2ZSA9IHRydWU7XG4gICAgICBpZiAoZS50eXBlID09PSBcInRvdWNoc3RhcnRcIikge1xuICAgICAgICBpbnRpYWxNb3VzZVBvc2l0aW9uID0gZS50b3VjaGVzWzBdLmNsaWVudFg7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpbnRpYWxNb3VzZVBvc2l0aW9uID0gZS5jbGllbnRYO1xuICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIGRyYWdFbmQoZSkge1xuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgIGlmIChhY3RpdmUpIHtcbiAgICAgICAgYWN0aXZlID0gZmFsc2U7XG4gICAgICAgIGxldCBmaW5hbFBvc2l0aW9uID1cbiAgICAgICAgICAgIGdldFRyYW5zbGF0ZVgoc2xpZGVzQ29udGFpbmVyWzBdKSA+IC0xMFxuICAgICAgICAgICAgICA/IDBcbiAgICAgICAgICAgICAgOiBnZXRUcmFuc2xhdGVYKHNsaWRlc0NvbnRhaW5lclswXSksXG4gICAgICAgICAgcGVyY2VudGFnZSA9IC1NYXRoLnJvdW5kKChmaW5hbFBvc2l0aW9uICogMTAwKSAvIHdpZHRoKTtcbiAgICAgICAgaWYgKGZpbmFsUG9zaXRpb24gPCAwKSB7XG4gICAgICAgICAgaWYgKGRpcmVjdGlvbiA9PT0gXCJsZWZ0XCIpIHtcbiAgICAgICAgICAgIHZhciB0cmFuc2l0aW9uRHVyYXRpb24gPVxuICAgICAgICAgICAgICAgIGRpcmVjdGlvbiA9PT0gXCJsZWZ0XCJcbiAgICAgICAgICAgICAgICAgID8gKDEgLSBwZXJjZW50YWdlIC8gMTAwKSAqIGR1cmF0aW9uXG4gICAgICAgICAgICAgICAgICA6IChwZXJjZW50YWdlICogZHVyYXRpb24pIC8gMTAwLFxuICAgICAgICAgICAgICBmcmFtZSA9XG4gICAgICAgICAgICAgICAgKCgxIC0gcGVyY2VudGFnZSAvIDEwMCkgKiB3aWR0aCkgL1xuICAgICAgICAgICAgICAgICgodHJhbnNpdGlvbkR1cmF0aW9uICogMTAwMCkgLyAxNik7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHZhciB0cmFuc2l0aW9uRHVyYXRpb24gPSAocGVyY2VudGFnZSAqIGR1cmF0aW9uKSAvIDEwMCxcbiAgICAgICAgICAgICAgZnJhbWUgPVxuICAgICAgICAgICAgICAgIChwZXJjZW50YWdlICogd2lkdGgpIC8gMTAwIC8gKCh0cmFuc2l0aW9uRHVyYXRpb24gKiAxMDAwKSAvIDE2KTtcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgc2xpZGVzQ29udGFpbmVyLmNzcyhcInRyYW5zZm9ybVwiLCBgdHJhbnNsYXRlWCgkezB9cHgpYCk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoZGlyZWN0aW9uID09PSBcImxlZnRcIikge1xuICAgICAgICAgIHRpbWVyID0gc2V0SW50ZXJ2YWwoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBsZXQgY3VyZW50UG9zaXRpb24gPSBnZXRUcmFuc2xhdGVYKHNsaWRlc0NvbnRhaW5lclswXSk7XG4gICAgICAgICAgICBpZiAoLWN1cmVudFBvc2l0aW9uID49IHdpZHRoKSB7XG4gICAgICAgICAgICAgIGNsZWFySW50ZXJ2YWwodGltZXIpO1xuICAgICAgICAgICAgICBzbGlkZXNDb250YWluZXIuYWRkQ2xhc3MoXCJtYXJxdWVlLWJhY2tcIik7XG4gICAgICAgICAgICAgIHNsaWRlc0NvbnRhaW5lci5hZGRDbGFzcyhcInBsYXlcIik7XG4gICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgIHNsaWRlc0NvbnRhaW5lci5jc3MoXG4gICAgICAgICAgICAgICAgXCJ0cmFuc2Zvcm1cIixcbiAgICAgICAgICAgICAgICBgdHJhbnNsYXRlWCgke2N1cmVudFBvc2l0aW9uIC0gZnJhbWV9cHgpYFxuICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sIDE2KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0aW1lciA9IHNldEludGVydmFsKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbGV0IGN1cmVudFBvc2l0aW9uID0gZ2V0VHJhbnNsYXRlWChzbGlkZXNDb250YWluZXJbMF0pO1xuICAgICAgICAgICAgaWYgKGN1cmVudFBvc2l0aW9uID49IDApIHtcbiAgICAgICAgICAgICAgY2xlYXJJbnRlcnZhbCh0aW1lcik7XG4gICAgICAgICAgICAgIHNsaWRlc0NvbnRhaW5lci5hZGRDbGFzcyhcIm1hcnF1ZWVcIik7XG4gICAgICAgICAgICAgIHNsaWRlc0NvbnRhaW5lci5hZGRDbGFzcyhcInBsYXlcIik7XG4gICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgIHNsaWRlc0NvbnRhaW5lci5jc3MoXG4gICAgICAgICAgICAgICAgXCJ0cmFuc2Zvcm1cIixcbiAgICAgICAgICAgICAgICBgdHJhbnNsYXRlWCgke2ZyYW1lICsgY3VyZW50UG9zaXRpb259cHgpYFxuICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sIDE2KTtcbiAgICAgICAgfVxuICAgICAgICBzbGlkZXNDb250YWluZXIub2ZmKFwidHJhbnNpdGlvbmVuZFwiKTtcbiAgICAgICAgc2xpZGVzQ29udGFpbmVyLm9uKFwidHJhbnNpdGlvbmVuZFwiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICBzbGlkZXNDb250YWluZXIucmVtb3ZlQ2xhc3MoXCJmaW5pc2hcIik7XG4gICAgICAgICAgc2xpZGVzQ29udGFpbmVyLnJlbW92ZUNsYXNzKFwic3RhcnRcIik7XG4gICAgICAgICAgc2xpZGVzQ29udGFpbmVyLmNzcyhcInRyYW5zaXRpb24tZHVyYXRpb25cIiwgYCR7MH1zYCk7XG4gICAgICAgICAgaWYgKGRpcmVjdGlvbiA9PT0gXCJyaWdodFwiKSB7XG4gICAgICAgICAgICBzbGlkZXNDb250YWluZXIuYWRkQ2xhc3MoXCJtYXJxdWVlXCIpO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBzbGlkZXNDb250YWluZXIuYWRkQ2xhc3MoXCJtYXJxdWVlLWJhY2tcIik7XG4gICAgICAgICAgfVxuICAgICAgICAgIHNsaWRlc0NvbnRhaW5lci5hZGRDbGFzcyhcInBsYXlcIik7XG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH1cbiAgICBmdW5jdGlvbiBtb3VzZUxlYXZlKGUpIHtcbiAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICBpZiAoYWN0aXZlKSB7XG4gICAgICAgIHNsaWRlc0NvbnRhaW5lci50cmlnZ2VyKFwibW91c2V1cFwiKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBkcmFnKGUpIHtcbiAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICBsZXQgZHJhZ1g7XG4gICAgICBpZiAoYWN0aXZlKSB7XG4gICAgICAgIGlmIChlLnR5cGUgPT09IFwidG91Y2htb3ZlXCIpIHtcbiAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgbGV0IGVsZW1lbnQgPSAkKFxuICAgICAgICAgICAgZG9jdW1lbnQuZWxlbWVudEZyb21Qb2ludChcbiAgICAgICAgICAgICAgZS50b3VjaGVzWzBdLmNsaWVudFgsXG4gICAgICAgICAgICAgIGUudG91Y2hlc1swXS5jbGllbnRZXG4gICAgICAgICAgICApXG4gICAgICAgICAgKTtcbiAgICAgICAgICBpZiAoIWVsZW1lbnQuaXMoc2xpZGVzQ29udGFpbmVyKSkge1xuICAgICAgICAgICAgc2xpZGVzQ29udGFpbmVyLnRyaWdnZXIoXCJ0b3VjaGVuZFwiKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZHJhZ1ggPSBlLnRvdWNoZXNbMF0uY2xpZW50WDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBkcmFnWCA9IGUuY2xpZW50WDtcbiAgICAgICAgfVxuICAgICAgICBsZXQgZGVsdGEgPSAtZHJhZ1ggKyBpbnRpYWxNb3VzZVBvc2l0aW9uLFxuICAgICAgICAgIHBvc2l0aW9uID0gY3VycmVudFggLSBkZWx0YTtcbiAgICAgICAgaWYgKGRyYWdYID4gaW50aWFsTW91c2VQb3NpdGlvbikge1xuICAgICAgICAgIGRpcmVjdGlvbiA9IFwicmlnaHRcIjtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBkaXJlY3Rpb24gPSBcImxlZnRcIjtcbiAgICAgICAgfVxuICAgICAgICBpZiAocG9zaXRpb24gPCAwICYmIHBvc2l0aW9uID4gLXdpZHRoKVxuICAgICAgICAgIHNsaWRlc0NvbnRhaW5lci5jc3MoXCJ0cmFuc2Zvcm1cIiwgYHRyYW5zbGF0ZVgoJHtwb3NpdGlvbn1weClgKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGRyYWdFbmQoe1xuICAgICAgICAgIGNsaWVudFg6IDAsXG4gICAgICAgICAgcHJldmVudERlZmF1bHQoKSB7fSxcbiAgICAgICAgICBzdG9wUHJvcGFnYXRpb24oKSB7fVxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbmV4cG9ydCBkZWZhdWx0IHNsaWRlcjtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9hc3NldHMvanMvcGFydGlhbHMvc2xpZGVyLmpzIiwiZnVuY3Rpb24gbW9kYWxzKCkge1xuICBsZXQgdHJpZ2dlcnMgPSAkKFwiW2RhdGEtb3Blbi1tb2RhbF1cIiksXG4gICAgY2xvc2VNb2RhbCA9ICQoXCJbZGF0YS1jbG9zZS1tb2RhbF1cIiksXG4gICAgYm9keSA9ICQoXCJib2R5XCIpO1xuICB0cmlnZ2Vycy5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKCkge1xuICAgIGxldCAkdGhpcyA9ICQodGhpcyksXG4gICAgICB0YXJnZXQgPSAkdGhpcy5kYXRhKFwibW9kYWxcIiksXG4gICAgICBtb2RhbCA9ICQoYC5tZWRpYS1tb2RhbFtkYXRhLW1vZGFsPVwiJHt0YXJnZXR9XCJdYCk7XG5cbiAgICBtb2RhbC5mYWRlSW4oXCJmYXN0XCIsIGZ1bmN0aW9uKCkge1xuICAgICAgaWYgKHRhcmdldCA9PT0gXCJ2aWRlb1wiKSB7XG4gICAgICAgIGxldCBpZCA9ICR0aGlzLmRhdGEoXCJpZFwiKSxcbiAgICAgICAgICBzb3VyY2UgPSAkdGhpcy5kYXRhKFwic291cmNlXCIpLFxuICAgICAgICAgIHZpZGVvID0gbW9kYWwuZmluZChcIi5tZWRpYS1tb2RhbF9fdmlkZW9cIik7XG4gICAgICAgIGlmICghdmlkZW8uYXR0cihcInNyY1wiKSkge1xuICAgICAgICAgIGxldCB1cmwgPSBcIlwiO1xuICAgICAgICAgIGlmIChzb3VyY2UgPT09IFwieW91dHViZVwiKSB7XG4gICAgICAgICAgICB1cmwgPSBgaHR0cHM6Ly93d3cueW91dHViZS5jb20vZW1iZWQvJHtpZH0/cmVsPTAmc2hvd2luZm89MCZhdXRvcGxheT0xYDtcbiAgICAgICAgICB9IGVsc2UgaWYgKHNvdXJjZSA9PT0gXCJ3aXN0aWFcIikge1xuICAgICAgICAgICAgdXJsID0gYC8vZmFzdC53aXN0aWEubmV0L2VtYmVkL2lmcmFtZS8ke2lkfT92aWRlb0ZvYW09dHJ1ZSZhdXRvUGxheT10cnVlJnBsYXllckNvbG9yPWZiMDAwMWA7XG4gICAgICAgICAgfSBlbHNlIGlmIChzb3VyY2UgPT09IFwidmltZW9cIikge1xuICAgICAgICAgICAgdXJsID0gYGh0dHBzOi8vcGxheWVyLnZpbWVvLmNvbS92aWRlby8ke2lkfT9hdXRvcGxheT0xJmxvb3A9MSZhdXRvcGF1c2U9MGA7XG4gICAgICAgICAgfVxuICAgICAgICAgIHZpZGVvLmF0dHIoXCJzcmNcIiwgdXJsKTtcbiAgICAgICAgfVxuICAgICAgICAvLyB2aWRlb1swXS5wbGF5KCk7XG4gICAgICB9XG4gICAgfSk7XG4gIH0pO1xuICBjbG9zZU1vZGFsLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oKSB7XG4gICAgbGV0ICR0aGlzID0gJCh0aGlzKSxcbiAgICAgIG1vZGFsID0gJHRoaXMucGFyZW50cyhgLm1lZGlhLW1vZGFsYCksXG4gICAgICB0eXBlID0gbW9kYWwuZGF0YShcIm1vZGFsXCIpO1xuICAgIGlmICh0eXBlID09PSBcInZpZGVvXCIpIHtcbiAgICAgIG1vZGFsLmZpbmQoXCIubWVkaWEtbW9kYWxfX3ZpZGVvXCIpLmF0dHIoXCJzcmNcIiwgXCJcIik7XG4gICAgfVxuICAgIG1vZGFsLmZhZGVPdXQoXCJmYXNlXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgYm9keS5yZW1vdmVDbGFzcyhcIm5vLXNjcm9sbFwiKTtcbiAgICB9KTtcbiAgfSk7XG59XG5leHBvcnQgZGVmYXVsdCBtb2RhbHM7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvYXNzZXRzL2pzL3BhcnRpYWxzL21vZGFscy5qcyIsImltcG9ydCB7IGxvYWRpbmcgfSBmcm9tIFwiLi4vYXBwXCI7XG5cbmZ1bmN0aW9uIG1ha2VNb2RhbEltYWdlKGltYWdlLCBmaXJzdCkge1xuICByZXR1cm4gYDxkaXYgY2xhc3M9XCJnYWxsZXJ5LXBhZ2VfX21vZGFsX19pbWFnZSAke2ZpcnN0ID8gXCJhY3RpdmVcIiA6IFwiXCJ9XCI+XG4gICAgPGltZyBzcmM9XCIke2ltYWdlLnVybH1cIiBhbHQ9XCIke2ltYWdlLm5hbWV9XCI+PC9kaXY+YDtcbn1cblxuZnVuY3Rpb24gZ2FsbGVyeSgpIHtcbiAgbGV0IGdhbGxlcnlGaWx0ZXJzID0gJChcIi5nYWxsZXJ5LXBhZ2VfX2ZpbHRlclwiKTtcbiAgaWYgKGdhbGxlcnlGaWx0ZXJzLmxlbmd0aCkge1xuICAgIGxldCBpbWFnZXNDb250YWluZXIgPSAkKFwiLmdhbGxlcnktcGFnZV9faW1hZ2VzXCIpLFxuICAgICAgZ2FsbGVyeUltYWdlcyA9ICQoXCIuZ2FsbGVyeS1wYWdlX19pbWFnZVwiKSxcbiAgICAgIG1vZGFsSW1hZ2VzVHJhY2sgPSAkKFwiLmdhbGxlcnktcGFnZV9fbW9kYWxfX2ltYWdlcy10cmFja1wiKSxcbiAgICAgIG1vZGFsQ2FwdGlvbnMgPSAkKFwiLmdhbGxlcnktcGFnZV9fbW9kYWxfX2NhcHRpb25cIiksXG4gICAgICBtb2RhbENvdW50RnJvbSA9ICQoXCIuZ2FsbGVyeS1wYWdlX19tb2RhbF9fY291bnQgLmN1cnJlbnRcIiksXG4gICAgICBtb2RhbENvdW50VG8gPSAkKFwiLmdhbGxlcnktcGFnZV9fbW9kYWxfX2NvdW50IC50b3RhbFwiKSxcbiAgICAgIGNvbnRyb2xzID0gJChcIi5nYWxsZXJ5LXBhZ2VfX21vZGFsX19jb250cm9sXCIpLFxuICAgICAgcHJldiA9ICQoXCIuZ2FsbGVyeS1wYWdlX19tb2RhbF9fY29udHJvbC5wcmV2XCIpLFxuICAgICAgbmV4dCA9ICQoXCIuZ2FsbGVyeS1wYWdlX19tb2RhbF9fY29udHJvbC5uZXh0XCIpLFxuICAgICAgbW9kYWxJbWFnZXMgPSAkKFwiLmdhbGxlcnktcGFnZV9fbW9kYWxfX2ltYWdlXCIpLFxuICAgICAgbW9kYWxJbWFnZXNOdW1iZXIgPSBtb2RhbEltYWdlcy5sZW5ndGgsXG4gICAgICBnYWxsZXJ5TW9kYWwgPSAkKFwiLm1lZGlhLW1vZGFsXCIpLFxuICAgICAgZ2FsbGVyeU1vZGFsSW1hZ2VXaWR0aCA9ICQoXCIuZ2FsbGVyeS1wYWdlX19tb2RhbF9faW1hZ2VcIikub3V0ZXJXaWR0aCgpLFxuICAgICAgaW5mbyA9IGltYWdlc0NvbnRhaW5lci5kYXRhKFwiaW5mb1wiKSxcbiAgICAgIGN1cnJlbnRGaWx0ZXIgPSBcIlwiLFxuICAgICAgb3BlbmVkTW9kYWwgPSBmYWxzZSxcbiAgICAgIG1vYmlsZUZpbHRlcnNCdG4gPSAkKFwiLmdhbGxlcnktcGFnZV9fZmlsdGVycy1leHBhbmQtYnRuXCIpLFxuICAgICAgZmlsdGVyc0NvbnRhaW5lciA9ICQoXCIuZ2FsbGVyeS1wYWdlX19maWx0ZXItY29udGFpbmVyXCIpO1xuICAgIGZ1bmN0aW9uIGdvdG9TbGlkZShpbmRleCkge1xuICAgICAgbGV0IGFjdGl2ZUltYWdlID0gJChcIi5nYWxsZXJ5LXBhZ2VfX21vZGFsX19pbWFnZS5hY3RpdmVcIik7XG4gICAgICBhY3RpdmVJbWFnZS5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTtcbiAgICAgIG1vZGFsSW1hZ2VzVHJhY2suY3NzKFxuICAgICAgICBcInRyYW5zZm9ybVwiLFxuICAgICAgICBgdHJhbnNsYXRlWCgkey1pbmRleCAqIGdhbGxlcnlNb2RhbEltYWdlV2lkdGh9cHgpYFxuICAgICAgKTtcbiAgICAgIG1vZGFsQ2FwdGlvbnMuaHRtbChpbmZvW2luZGV4XS5uYW1lKTtcbiAgICAgIG1vZGFsQ291bnRGcm9tLmh0bWwoaW5kZXggKyAxKTtcbiAgICAgIG1vZGFsSW1hZ2VzLmVxKGluZGV4KS5hZGRDbGFzcyhcImFjdGl2ZVwiKTtcblxuICAgICAgY29udHJvbHMucmVtb3ZlQ2xhc3MoXCJkaXNhYmxlZFwiKTtcbiAgICAgIGlmIChpbmRleCArIDEgPT09IG1vZGFsSW1hZ2VzTnVtYmVyKSB7XG4gICAgICAgIG5leHQuYWRkQ2xhc3MoXCJkaXNhYmxlZFwiKTtcbiAgICAgIH1cbiAgICAgIGlmIChpbmRleCA9PT0gMCkge1xuICAgICAgICBwcmV2LmFkZENsYXNzKFwiZGlzYWJsZWRcIik7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaW1hZ2VzQ29udGFpbmVyLm9uKFwiY2xpY2tcIiwgXCIuZ2FsbGVyeS1wYWdlX19pbWFnZVwiLCBmdW5jdGlvbigpIHtcbiAgICAgIGxldCAkdGhpcyA9ICQodGhpcyksXG4gICAgICAgIGluZGV4ID0gZ2FsbGVyeUltYWdlcy5pbmRleCgkdGhpcyk7XG4gICAgICBpZiAoIW9wZW5lZE1vZGFsKSB7XG4gICAgICAgIGxldCBpbWFnZXMgPSBcIlwiO1xuICAgICAgICBmb3IgKHZhciBpbWFnZSBpbiBpbmZvKSB7XG4gICAgICAgICAgaW1hZ2VzICs9IG1ha2VNb2RhbEltYWdlKGluZm9baW1hZ2VdLCBpbWFnZSA9PT0gMCA/IHRydWUgOiBmYWxzZSk7XG4gICAgICAgIH1cbiAgICAgICAgbW9kYWxJbWFnZXNUcmFjay5odG1sKGltYWdlcyk7XG4gICAgICAgIG1vZGFsSW1hZ2VzID0gJChcIi5nYWxsZXJ5LXBhZ2VfX21vZGFsX19pbWFnZVwiKTtcbiAgICAgICAgbW9kYWxJbWFnZXNOdW1iZXIgPSBtb2RhbEltYWdlcy5sZW5ndGg7XG4gICAgICAgIG1vZGFsQ291bnRGcm9tLmh0bWwoaW5kZXggKyAxKTtcbiAgICAgICAgbW9kYWxDb3VudFRvLmh0bWwobW9kYWxJbWFnZXNOdW1iZXIpO1xuICAgICAgICBvcGVuZWRNb2RhbCA9IHRydWU7XG4gICAgICB9XG4gICAgICBnb3RvU2xpZGUoaW5kZXgpO1xuICAgICAgZ2FsbGVyeU1vZGFsLmZhZGVJbig1MDApO1xuICAgIH0pO1xuXG4gICAgZ2FsbGVyeUZpbHRlcnMub24oXCJjbGlja1wiLCBmdW5jdGlvbigpIHtcbiAgICAgIGxldCAkdGhpcyA9ICQodGhpcyksXG4gICAgICAgIGFjdGl2ZUZpbHRlciA9ICQoXCIuZ2FsbGVyeS1wYWdlX19maWx0ZXIuYWN0aXZlXCIpO1xuICAgICAgaWYgKCEkdGhpcy5oYXNDbGFzcyhcImFjdGl2ZVwiKSkge1xuICAgICAgICBsZXQgZmlsdGVyID0gJHRoaXMuZGF0YShcImNhdFwiKTtcbiAgICAgICAgaW1hZ2VzQ29udGFpbmVyLmh0bWwobG9hZGluZyk7XG4gICAgICAgICQuZ2V0KGAvZ2FsbGVyeS8/ZmlsdGVyPSR7ZmlsdGVyfWApLnRoZW4ocmVzcCA9PiB7XG4gICAgICAgICAgbGV0ICRyZXNwID0gJChyZXNwKSxcbiAgICAgICAgICAgIG5ld0luZm8gPSAkcmVzcC5maW5kKFwiLmdhbGxlcnktcGFnZV9faW1hZ2VzXCIpLmRhdGEoXCJpbmZvXCIpO1xuICAgICAgICAgIGluZm8gPSBuZXdJbmZvO1xuICAgICAgICAgIGN1cnJlbnRGaWx0ZXIgPSBmaWx0ZXI7XG4gICAgICAgICAgaW1hZ2VzQ29udGFpbmVyLmh0bWwoJHJlc3AuZmluZChcIi5nYWxsZXJ5LXBhZ2VfX2ltYWdlXCIpKTtcbiAgICAgICAgICBpbWFnZXNDb250YWluZXIuZGF0YShcImluZm9cIiwgbmV3SW5mbyk7XG4gICAgICAgICAgYWN0aXZlRmlsdGVyLnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpO1xuICAgICAgICAgICR0aGlzLmFkZENsYXNzKFwiYWN0aXZlXCIpO1xuICAgICAgICAgIG9wZW5lZE1vZGFsID0gZmFsc2U7XG4gICAgICAgICAgZ2FsbGVyeUltYWdlcyA9ICQoXCIuZ2FsbGVyeS1wYWdlX19pbWFnZVwiKTtcbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfSk7XG5cbiAgICBjb250cm9scy5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgbGV0ICR0aGlzID0gJCh0aGlzKSxcbiAgICAgICAgaW5kZXggPSBtb2RhbEltYWdlcy5pbmRleCgkKFwiLmdhbGxlcnktcGFnZV9fbW9kYWxfX2ltYWdlLmFjdGl2ZVwiKSk7XG4gICAgICBpZiAoJHRoaXMuaGFzQ2xhc3MoXCJuZXh0XCIpKSB7XG4gICAgICAgIGlmIChpbmRleCA8IG1vZGFsSW1hZ2VzTnVtYmVyIC0gMSkge1xuICAgICAgICAgIGdvdG9TbGlkZShpbmRleCArIDEpO1xuICAgICAgICAgIGluZGV4ICsgMSA9PT0gbW9kYWxJbWFnZXNOdW1iZXIgLSAxXG4gICAgICAgICAgICA/ICR0aGlzLmFkZENsYXNzKFwiZGlzYWJsZWRcIilcbiAgICAgICAgICAgIDogY29udHJvbHMucmVtb3ZlQ2xhc3MoXCJkaXNhYmxlZFwiKTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIGlmICgkdGhpcy5oYXNDbGFzcyhcInByZXZcIikpIHtcbiAgICAgICAgaWYgKGluZGV4ID4gMCkge1xuICAgICAgICAgIGdvdG9TbGlkZShpbmRleCAtIDEpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSk7XG4gICAgbW9iaWxlRmlsdGVyc0J0bi5vbihcImNsaWNrXCIsIGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgbGV0ICR0aGlzID0gJCh0aGlzKSxcbiAgICAgICAgaGVpZ2h0O1xuICAgICAgaWYgKCFmaWx0ZXJzQ29udGFpbmVyLmRhdGEoXCJoZWlnaHRcIikpIHtcbiAgICAgICAgaGVpZ2h0ID0gZmlsdGVyc0NvbnRhaW5lclswXS5zY3JvbGxIZWlnaHQ7XG4gICAgICAgIGZpbHRlcnNDb250YWluZXIuZGF0YShcImhlaWdodFwiLCBoZWlnaHQpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaGVpZ2h0ID0gZmlsdGVyc0NvbnRhaW5lci5kYXRhKFwiaGVpZ2h0XCIpO1xuICAgICAgfVxuICAgICAgaWYgKGZpbHRlcnNDb250YWluZXIuaGFzQ2xhc3MoXCJvcGVuXCIpKSB7XG4gICAgICAgIGZpbHRlcnNDb250YWluZXIuY3NzKFwibWF4LWhlaWdodFwiLCAwKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGZpbHRlcnNDb250YWluZXIuY3NzKFwibWF4LWhlaWdodFwiLCBgJHtoZWlnaHR9cHhgKTtcbiAgICAgIH1cbiAgICAgIGZpbHRlcnNDb250YWluZXIudG9nZ2xlQ2xhc3MoXCJvcGVuXCIpO1xuICAgIH0pO1xuICB9XG59XG5leHBvcnQgZGVmYXVsdCBnYWxsZXJ5O1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2Fzc2V0cy9qcy9wYXJ0aWFscy9nYWxsZXJ5LmpzIiwiZnVuY3Rpb24gYWNjZXNzb3JpZXMoKSB7XG4gIGxldCBhY2Nlc3NvcnlNb2RhbFRyaWdnZXJzID0gJChcIltkYXRhLW9wZW4tYWNjZXNzb3J5LW1vZGFsXVwiKTtcbiAgaWYgKGFjY2Vzc29yeU1vZGFsVHJpZ2dlcnMubGVuZ3RoKSB7XG4gICAgbGV0IG1vZGFsID0gJChcIi5hY2Nlc3Nvcmllcy1tb2RhbFwiKSxcbiAgICAgIG1vZGFsSW1hZ2UgPSAkKFwiLmFjY2Vzc29yaWVzLW1vZGFsX19pbWFnZVwiKSxcbiAgICAgIG1vZGFsVGl0bGUgPSAkKFwiLmFjY2Vzc29yaWVzLW1vZGFsX190aXRsZVwiKSxcbiAgICAgIG1vZGFsRmVhdHVyZXMgPSAkKFwiLmFjY2Vzc29yaWVzLW1vZGFsX19mZWF0dXJlc1wiKSxcbiAgICAgIG1vZGFsRGVzYyA9ICQoXCIuYWNjZXNzb3JpZXMtbW9kYWxfX2Rlc2NcIiksXG4gICAgICBib2R5ID0gJChcImJvZHlcIik7XG5cbiAgICBhY2Nlc3NvcnlNb2RhbFRyaWdnZXJzLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oKSB7XG4gICAgICBsZXQgYWNjZXNzb3J5ID0gJCh0aGlzKS5wYXJlbnRzKFwiLmFjY2Vzc29yeVwiKSxcbiAgICAgICAgYWNjZXNzb3J5VGl0bGUgPSBhY2Nlc3NvcnkuZmluZChcIi5hY2Nlc3NvcnlfX3RpdGxlXCIpLFxuICAgICAgICBhY2Nlc3NvcnlJbWFnZSA9IGFjY2Vzc29yeS5maW5kKFwiLmFjY2Vzc29yeV9faW1hZ2VcIiksXG4gICAgICAgIGFjY2Vzc29yeUZlYXR1cmVzID0gYWNjZXNzb3J5LmZpbmQoXCIuYWNjZXNzb3J5X19mZWF0dXJlc1wiKSxcbiAgICAgICAgYWNjZXNzb3J5RGVzYyA9IGFjY2Vzc29yeS5maW5kKFwiLmFjY2Vzc29yeV9fZGVzY1wiKTtcbiAgICAgIG1vZGFsSW1hZ2UuYXR0cih7XG4gICAgICAgIHNyYzogYWNjZXNzb3J5SW1hZ2UuYXR0cihcInNyY1wiKSxcbiAgICAgICAgYWx0OiBhY2Nlc3NvcnlJbWFnZS5hdHRyKFwiYWx0XCIpXG4gICAgICB9KTtcbiAgICAgIG1vZGFsVGl0bGUuaHRtbChhY2Nlc3NvcnlUaXRsZS5odG1sKCkpO1xuICAgICAgbW9kYWxGZWF0dXJlcy5odG1sKGFjY2Vzc29yeUZlYXR1cmVzLmh0bWwoKSk7XG4gICAgICBtb2RhbERlc2MuaHRtbChhY2Nlc3NvcnlEZXNjLmh0bWwoKSk7XG4gICAgICBtb2RhbC5mYWRlSW4oNTAwLCAoKSA9PiB7XG4gICAgICAgIGJvZHkuYWRkQ2xhc3MoXCJuby1zY3JvbGxcIik7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBhY2Nlc3NvcmllcztcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9hc3NldHMvanMvcGFydGlhbHMvYWNjZXNzb3JpZXMuanMiLCJmdW5jdGlvbiBzeXN0ZW0oKSB7XG4gIGxldCBpbWFnZXMgPSAkKFwiLnN5c3RlbS1wYWdlX19nYWxsZXJ5X19pbWFnZVwiKSxcbiAgICBpbWFnZXNOdW1iZXIgPSBpbWFnZXMubGVuZ3RoLFxuICAgIG1vZGVsc1RhYmxlcyA9ICQoXCIuc3lzdGVtLXBhZ2VfX21vZGVsc19fdGFibGVcIik7XG4gIGlmIChpbWFnZXNOdW1iZXIpIHtcbiAgICBsZXQgY29udHJvbHMgPSAkKFwiLnN5c3RlbS1wYWdlX19nYWxsZXJ5X19jb250cm9sXCIpO1xuICAgIGNvbnRyb2xzLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oKSB7XG4gICAgICBsZXQgJHRoaXMgPSAkKHRoaXMpLFxuICAgICAgICBhY3RpdmVJbWFnZSA9ICQoXCIuc3lzdGVtLXBhZ2VfX2dhbGxlcnlfX2ltYWdlLmFjdGl2ZVwiKSxcbiAgICAgICAgaW5kZXggPSBpbWFnZXMuaW5kZXgoYWN0aXZlSW1hZ2UpO1xuXG4gICAgICBpZiAoJHRoaXMuaGFzQ2xhc3MoXCJuZXh0XCIpKSB7XG4gICAgICAgIGlmIChpbmRleCA8IGltYWdlc051bWJlciAtIDEpIHtcbiAgICAgICAgICBpbWFnZXMuZXEoaW5kZXggKyAxKS5hZGRDbGFzcyhcImFjdGl2ZVwiKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBpbWFnZXMuZXEoMCkuYWRkQ2xhc3MoXCJhY3RpdmVcIik7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGlmIChpbmRleCA9PT0gMCkge1xuICAgICAgICAgIGltYWdlcy5lcShpbWFnZXNOdW1iZXIgLSAxKS5hZGRDbGFzcyhcImFjdGl2ZVwiKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBpbWFnZXMuZXEoaW5kZXggLSAxKS5hZGRDbGFzcyhcImFjdGl2ZVwiKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgYWN0aXZlSW1hZ2UucmVtb3ZlQ2xhc3MoXCJhY3RpdmVcIik7XG4gICAgfSk7XG4gIH1cbiAgaWYgKG1vZGVsc1RhYmxlcy5sZW5ndGgpIHtcbiAgICBsZXQgaGVpZ2h0ID0gbW9kZWxzVGFibGVzLmVxKDApLmhlaWdodCgpO1xuICAgIG1vZGVsc1RhYmxlcy5lcSgxKS5oZWlnaHQoaGVpZ2h0KTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBzeXN0ZW07XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvYXNzZXRzL2pzL3BhcnRpYWxzL3N5c3RlbS5qcyIsImZ1bmN0aW9uIGZvcm1zKCkge1xuICBsZXQgZm9ybUdyb3VwID0gJChcIi5mb3JtLWdyb3VwXCIpO1xuICBmb3JtR3JvdXAub24oXCJjbGlja1wiLCBmdW5jdGlvbigpIHtcbiAgICBsZXQgaW5wdXQgPSAkKHRoaXMpLmZpbmQoXCJpbnB1dFwiKTtcbiAgICBpbnB1dC5mb2N1cygpO1xuICB9KTtcbn1cbmV4cG9ydCBkZWZhdWx0IGZvcm1zO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2Fzc2V0cy9qcy9wYXJ0aWFscy9mb3Jtcy5qcyJdLCJzb3VyY2VSb290IjoiIn0=