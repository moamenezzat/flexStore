<?php


/** Enqueue scripts */
require_once( 'library/enqueue-scripts.php' );

/** Add support for featured image */
add_theme_support( 'post-thumbnails' );

// removeing embeds and emojis
function my_deregister_scripts(){
 wp_dequeue_script( 'wp-embed' );
}

add_action( 'wp_footer', 'my_deregister_scripts' );
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

//add SVG support
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function svg_meta_data($data, $id){

    $attachment = get_post($id); // Filter makes sure that the post is an attachment
    $mime_type = $attachment->post_mime_type; // The attachment mime_type

    //If the attachment is an svg

    if($mime_type == 'image/svg+xml'){

        //If the svg metadata are empty or the width is empty or the height is empty
        //then get the attributes from xml.

        if(empty($data) || empty($data['width']) || empty($data['height'])){

            $xml = simplexml_load_file(wp_get_attachment_url($id));
            $attr = $xml->attributes();
            $viewbox = explode(' ', $attr->viewBox);
            $data['width'] = isset($attr->width) && preg_match('/\d+/', $attr->width, $value) ? (int) $value[0] : (count($viewbox) == 4 ? (int) $viewbox[2] : null);
            $data['height'] = isset($attr->height) && preg_match('/\d+/', $attr->height, $value) ? (int) $value[0] : (count($viewbox) == 4 ? (int) $viewbox[3] : null);
        }

    }

    return $data;

}
add_filter('wp_update_attachment_metadata', 'svg_meta_data', 10, 2);


// // add categories for attachments
// function add_categories_for_attachments() {
//     register_taxonomy_for_object_type( 'category', 'attachment' );
// }
// add_action( 'init' , 'add_categories_for_attachments' );
//
// // add tags for attachments
// function add_tags_for_attachments() {
//     register_taxonomy_for_object_type( 'post_tag', 'attachment' );
// }
// add_action( 'init' , 'add_tags_for_attachments' );

//move gravity forms scripts to footer
add_filter('gform_init_scripts_footer', '__return_true');


//add class to gravity form submit
add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
function form_submit_button($button, $form) {
    return '<button type="submit" class="button cta" id="gform_submit_button_' . $form['id'] . '" ><span>'.$form['button']['text'] .'</span></button>';
}


//add logos shortcode
function replace_logo($atts) {
  extract(shortcode_atts(array(
      'logo' =>"FlexStor",
   ), $atts));
   $return_string = "<img class='replaced-logo' src='".get_template_directory_uri()."/dist/assets/images/".$logo.".svg' alt='".$logo."'  />";
   return $return_string;
}

function register_shortcodes(){
   add_shortcode('replace_logo', 'replace_logo');
}
add_action( 'init', 'register_shortcodes');


// //Remove WPAUTOP from ACF TinyMCE Editor
// function acf_wysiwyg_remove_wpautop() {
//     remove_filter('acf_the_content', 'wpautop' );
// }
// add_action('acf/init', 'acf_wysiwyg_remove_wpautop');


//striped ACF field
function striped_field($field){
  echo strip_tags(get_field($field),"<img><br>");
}

// set Editor height
add_action('acf/input/admin_head', 'my_acf_modify_wysiwyg_height');
function my_acf_modify_wysiwyg_height() {

    ?>
    <style type="text/css">
        .acf-input iframe{
            height: 50px !important;
        }
    </style>
    <?php

}


add_filter('next_posts_link_attributes', 'next_posts_link_attributes');
add_filter('previous_posts_link_attributes', 'prev_posts_link_attributes');

function next_posts_link_attributes() {
    return 'class="nav-link next"';
}
function prev_posts_link_attributes() {
    return 'class="nav-link prev"';
}
