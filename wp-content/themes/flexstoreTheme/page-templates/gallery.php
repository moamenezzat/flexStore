<?php
/**
* Template Name: Gallery
*/

get_header();

$filter = $_GET["filter"];


function filterRows($row) {
  global $filter;
  if($row["category"]==$filter){
    return true;
  }
}


$imagesRows = get_field("gallery");
foreach ($imagesRows as $key => &$image) {
  get_cat($key);
}

function get_images_info ($images) {
  $imagesObject =new stdClass();
   foreach($images as $key=>$image){
    $imagesObject->{$key} = (object) [url =>$image["url"], name =>  $image[caption]];
   }
  return json_encode($imagesObject);
};


function get_cat($index){
  global  $imagesRows;
  $cat = get_field("category", $imagesRows[$index][ID]);
  $imagesRows[$index]["category"] = $cat;
}
$images = null;
if($filter){
  $images = array_values(array_filter($imagesRows,"filterRows"));
}else{
$images = $imagesRows;
}

$length = count($images);


 ?>

 <section class="gallery-page">
   <div class="page-header">
     <h1 class="page-title">
       <?php the_title(); ?>
     </h1>
     <div class=" gallery-page__filters">
        <button type="button" name="button" class="gallery-page__filters-expand-btn">Filter gallery</button>
        <div class="flex-container gallery-page__filter-container">
           <button type="button" name="button" class="gallery-page__filter active" data-cat="">All</button>
           <button type="button" name="button" class="gallery-page__filter" data-cat="FlexStor">FlexStor</button>
           <button type="button" name="button" class="gallery-page__filter" data-cat="FlexStand">FlexStand</button>
           <button type="button" name="button" class="gallery-page__filter" data-cat="Accessories">Accessories</button>
        </div>
     </div>
   </div>
   <div class="gallery-page__images flex-container flex-wrap" data-info='<?php echo get_images_info($images); ?>'>
     <?php for ($x = 0; $x<$length;$x++) { ?>

       <button class="gallery-page__image" style="background-image:url('<?php echo $images[$x]["sizes"]["medium_large"] ?>')">
         <div class="gallery-page__image__hover">
           <img src="<?php echo get_template_directory_uri();  ?>/dist/assets/images/icons/image.svg" alt="Image icon">
           <p>Enlarge</p>
         </div>
       </button>
    <?php } ?>
   </div>
 </section>
 <div class="media-modal gallery-page__modal">
     <button type="button" name="button" class="media-modal__close" data-close-modal>close</button>
     <div class="gallery-page__modal__images">
       <div class="gallery-page__modal__images-track flex-container align-center">
         <div class="gallery-page__modal__image">
           <img src="" alt="">
         </div>
       </div>
     </div>
     <div class="gallery-page__modal__footer flex-container align-center">
       <p class="gallery-page__modal__caption"></p>
       <p class="gallery-page__modal__count">
         <span class="current"></span> of <span class="total"></span>
       </p>
       <div class="flex-container gallery-page__modal__controls">
         <button class="gallery-page__modal__control prev" title="previous image" aria-label="previous image"><img src="<?php echo get_template_directory_uri();  ?>/dist/assets/images/icons/left.svg" alt="prev image icon"></button>
         <button class="gallery-page__modal__control next" title="next image" aria-label="next image"><img src="<?php echo get_template_directory_uri();  ?>/dist/assets/images/icons/left.svg" alt="next image icon"></button>
       </div>

     </div>
 </div>

<?php get_footer();
