<?php
/**
* Template Name: Home
*/

get_header();
 ?>
<section class="home-hero full-view-section flex-container flex-column justify-center" style="background-image: url('<?php the_field("background"); ?>')">
  <h1 class="home-hero__title"><?php the_field("title"); ?></h1>
  <p class="home-hero__subtitle"><?php the_field("subtitle"); ?></p>
  <button type="button" name="home-video" class="home-hero__video-btn" data-id="<?php the_field("video"); ?>" data-source="<?php the_field("video_hosting"); ?>" data-open-modal data-modal="video">Launch Video</button>
</section>
<section class="home-links flex-container">
  <a href="/systems" class="home-link systems">Storage<br>Systems</a>
  <a href="/accessories/" class="home-link acc">Storage<br>Accessories</a>
  <a href="/gallery" class="home-link gallery">Product<br>Gallery</a>
</section>
<section class="customers">
  <h2 class="customers__title">Proud<br>Customers</h2>
  <div class="customers__slider">
    <ul class="customers__slider__slide-container" >
      <?php
      $customers = get_field("customers");
      foreach ($customers as $key => $customer) {
       ?>
        <li class="customers__slider__slide">
            <img src="<?php echo $customer["url"] ?>" alt="<?php echo $customer["title"] ?>">
        </li>
      <?php } ?>
    </ul>
</div>
</section>
<div class="media-modal" data-modal="video">
  <button type="button" name="button" class="media-modal__close" data-close-modal>close</button>
   <iframe class="media-modal__video" src=""></iframe>
</div>
<?php get_footer();
