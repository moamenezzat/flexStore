<?php
/**
* Template Name: News
*/

get_header();

 ?>

 <section class="page-header news-archive">
   <h1 class="page-title">
     Latest News
   </h1>
 </section>
 <section class="news-articles">
   <div class="news-article-container  flex-container flex-wrap">
   <?php
       $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
       $args = array(
           'post_type' => 'post',
           'orderby' => 'post_date',
           'posts_per_page' => 9,
           'paged' => $paged
       );
       $query = new WP_Query($args);
       if($query->have_posts()): ?>
       <?php while ($query->have_posts()) : $query->the_post();
       ?>
       <a href="<?php the_permalink(); ?>" class="news-article">
         <div class="news-article__img" style="background-image: url('<?php echo get_the_post_thumbnail_url();?>')"></div>
         <h2 class="news-article__title">
           <?php the_title(); ?>
         </h2>
         <p class="news-article__date"> Posted on <strong><?php echo get_the_date(); ?></strong></p>
       </a>
       <?php
          endwhile;
          endif;
        wp_reset_postdata();
        ?>
    </div>
    <div class="news-articles__pagination">
      <?php
      next_posts_link('Next <span></span>', $query->max_num_pages );
      previous_posts_link( '<span></span> Previous' );
      ?>

    </div>
 </section>

 <a href="/quote" class="cta full-width "><span>Request a Free Quote</span></a>
 <?php get_footer();
