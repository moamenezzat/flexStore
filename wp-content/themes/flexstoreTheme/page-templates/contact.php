<?php
/**
* Template Name: Contact
*/

get_header();
 ?>
 <section class="flex-container contact-page">
   <div class="contact-page__form">
     <?php echo do_shortcode("[gravityform id=1 ajax=false]"); ?>
   </div>
   <div class="contact-page__locations">
     <div class="contact-page__locations-container">
     <?php
      if( have_rows('locations') ):
          while ( have_rows('locations') ) : the_row();
          $phone = get_sub_field('phone');
          $email = get_sub_field('email');
          $link = get_sub_field('website_link');
          ?>
            <div class="contact-page__location">
              <h3 class="contact-page__location__title"><?php the_sub_field("title") ?></h3>
              <p class="contact-page__location__subtitle"><?php the_sub_field("subtitle") ?></p>
              <div class="flex-container contact-page__location__contact flex-start">
                <p class="contact-page__location__address">
                  <?php the_sub_field("address") ?>
                </p>
                <div class="contact-page__location__contact-info-container">
                  <?php
                  if($phone){
                   ?>
                   <p class="contact-page__location__contact-info"><?php echo $phone; ?></p>
                 <?php } ?>
                  <?php
                  if($email){
                   ?>
                   <p class="contact-page__location__contact-info"><?php echo $email; ?></p>
                 <?php } ?>
                  <?php
                  if($link){
                   ?>
                   <a class="contact-page__location__contact-info link" href = "<?php echo $phone; ?>">Visit website</a>
                 <?php } ?>
                </div>
              </div>
            </div>
    <?php
      endwhile;
      endif;
      ?>
      </div>
   </div>

 </section>
 <?php get_footer();
