<?php
/**
* Template Name: Quote
*/

get_header();
 ?>
<section class="quote-page">
  <h1 class="page-title">
    <?php the_field("title") ?>
  </h1>
  <p class="quote-page__subtitle">
    <?php  striped_field("subtitle"); ?>
  </p>
  <p class="quote-page__headline">
    <?php the_field("headline") ?>
  </p>
  <div class="quote-form">
    <?php echo do_shortcode("[gravityform id=2  ajax=false title=false description=false]"); ?>
  </div>

</section>

<?php get_footer();
