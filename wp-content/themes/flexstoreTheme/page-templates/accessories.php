<?php
/**
* Template Name: Accessories
*/

get_header();

 ?>
   <section class="accessories-page__header">
     <h1><?php the_field("title") ?></h1>
   </section>
   <section class="accessories-container">
     <?php
      if( have_rows('accessories') ):
          while ( have_rows('accessories') ) : the_row()
      ?>
      <div class="accessory flex-container flex-start">
        <?php $image = get_sub_field("image"); ?>
        <?php  ?>
        <img src="<?php echo $image["sizes"]["medium_large"];?>" alt="<?php echo $image["name"];?>" class="accessory__image">
        <div class="accessory__info">
          <h2 class="accessory__title">
            <?php echo strip_tags(get_sub_field("title"),"<img>") ?> <span class="accessory__title__subtitle"><?php the_sub_field("subtitle") ?></span>
          </h2>
          <p class="accessory__headline"><?php the_sub_field("headline") ?></p>
          <ul class="accessory__features">
            <?php
             if( have_rows('list') ):
                 while ( have_rows('list') ) : the_row()
             ?>
             <li class="accessory__feature"><?php the_sub_field("feature") ?></li>
             <?php
             endwhile;
             endif;
             ?>
          </ul>
          <button class="cta" data-open-accessory-modal><span>Learn more</span></button>
          <div class="accessory__desc">
            <?php the_sub_field("description") ?>
          </div>
        </div>
      </div>
      <?php
      endwhile;
      endif;
      ?>
   </section>
   <div class="media-modal accessories-modal ">
     <div class="accessories-modal__container">
       <button class="media-modal__close" data-close-modal>Close</button>
       <div class="accessories-modal__inner-container">
         <div class="accessories-modal__header flex-container flex-start">
           <img src="" alt="" class="accessories-modal__image">
           <div class="accessories-modal__info">
             <h3 class="accessories-modal__title"></h3>
             <ul class="accessories-modal__features"></ul>
           </div>

         </div>
         <div class="accessories-modal__desc"></div>
         <a href="/quote" class="cta"><span>Request a free quote</span></a>
       </div>
     </div>
   </div>

<?php get_footer();
