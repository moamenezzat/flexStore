<?php
/**
* Template Name: Systems Display
*/

get_header();
 ?>
<section class="systems-page flex-container full-view-section">
  <div class="system flex-container flex-column justify-center align-center"
  style="background-image:url('<?php the_field("flexstor_bg"); ?>')">
    <img src="<?php echo get_template_directory_uri();?>/dist/assets/images/FlexStor.svg" alt="FlexStor" class="system__logo">
    <a class="cta" href="/flexstor"><span>View system</span></a>
  </div>
  <div class="system flex-container flex-column justify-center align-center"
  style="background-image:url('<?php the_field("flexstand_bg"); ?>')">
    <img src="<?php echo get_template_directory_uri();?>/dist/assets/images/FlexStand.svg" alt="FlexStand" class="system__logo">
    <a class="cta" href="/flexstand"><span>View system</span></a>
  </div>
</section>

<?php get_footer();
