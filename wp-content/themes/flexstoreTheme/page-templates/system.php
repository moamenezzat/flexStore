<?php
/**
* Template Name: System
*/

get_header();
$title = get_the_title();
 ?>
<section class="page-header system-page__header" style="background-image:linear-gradient(to left,rgba(42, 42, 42, 0.79),rgba(42, 42, 42, 0.79)),url('<?php the_field("header_bg") ?>')">
  <h1 class="system-page__title"><?php striped_field("title") ?></h1>
</section>
<section class="flex-container system-page__overview-section">
  <div class="system-page__gallery">
    <div class="system-page__gallery__images">
      <?php $images = get_field("gallery");
      foreach ($images as $key => $image) {
        ?>
        <div class="system-page__gallery__image <?php  echo ($key==0?  "active" :"");?>" style="background-image:url('<?php echo $image["url"]; ?>')"></div>
      <?php
      }
      ?>
    </div>
    <div class="system-page__gallery__controls">
    <button class="system-page__gallery__control gallery-page__modal__control prev"  title="previous image" aria-label="previous image">
      <img src="<?php echo get_template_directory_uri();  ?>/dist/assets/images/icons/left.svg" alt="prev image icon">
    </button>
    <button class="system-page__gallery__control gallery-page__modal__control next" title="next image" aria-label="next image">
      <img src="<?php echo get_template_directory_uri();  ?>/dist/assets/images/icons/left.svg" alt="next image icon">
    </button></div>
  </div>
  <div class="system-page__overview">
    <h3 class="system-page__overview__title"><?php echo do_shortcode("[replace_logo logo=".$title."]") ?>
    overview</h3>
    <div class="system-page__overview__content">
      <?php the_field("overview") ?>
    </div>
  </div>
</section>
<section class="system-page__info">
  <div class="system-page__features">
    <h3 class="section-title">
      Features
    </h3>
    <ul class="system-page__feature-list flex-container space-between">
      <?php
        if( have_rows('features') ):
            while ( have_rows('features') ) : the_row(); ?>
                <li class="system-page__feature">
                  <?php the_sub_field("feature") ?>
                </li>
          <?php  endwhile;
          endif;
        ?>
    </ul>
  </div>
  <?php
    if( have_rows('available_configurations') ){ ?>
    <div class="system-page__models">
      <div class="flex-container system-page__models__header">
        <div>
          <h3 class="section-title">
            Available<br>Configurations
        </h3>
        <a href="/quote" class="cta"><span>Inquire Today</span></a>
      </div>
      <img src="<?php $image = get_field("available_configurations_image");
      echo $image["sizes"]["large"]; ?>" alt="<?php echo $image["name"]; ?>" class="system-page__models__image">
    </div>
    <div class="flex-container system-page__models__tables-container flex-container">
      <table class="system-page__models__table">
        <thead>
          <tr>
            <th>model</th>
            <th colspan="2" class="two-col">
               <span>MODEL</span>
               <div class="flex-container"><span>IN</span><span>MM</span></div>
            </th>
            <th>HEIGHT</th>
          </tr>
        </thead>
        <tbody>
        <?php  while ( have_rows('available_configurations') ) : the_row(); ?>
               <tr>
                 <td><?php the_sub_field("model") ?></td>
                 <td class="cell-half"><?php the_sub_field("model_length_in") ?></td>
                 <td class="cell-half no-padding"><?php the_sub_field("model_length_mm") ?></td>
                 <td><?php the_sub_field("height") ?></td>
               </tr>
        <?php  endwhile; ?>
               </tbody>
            </table>
            <table class="system-page__models__table second-table">
              <thead>
                <tr>
                  <th class="gap"></th>
                  <th colspan="2" class="two-col">
                    <span>MAX. REPEAT</span>
                    <div class="flex-container"><span>IN</span><span>MM</span></div>
                  </th>
                  <th colspan="2" class="two-col">
                    <span>WIDTH</span>
                    <div class="flex-container"><span>IN</span><span>MM</span></div>
                  </th>
                </tr>
              </thead>
              <tbody>
              <?php  while ( have_rows('available_configurations') ) : the_row(); ?>
                    <tr>
                      <td class="gap"></td>
                      <td class="cell-half"><?php the_sub_field("max_repeat_in") ?></td>
                      <td class=" cell-half no-padding"><?php the_sub_field("max_repeat_mm") ?></td>
                      <td class="cell-half"><?php the_sub_field("width_in") ?></td>
                      <td class="cell-half no-padding"><?php the_sub_field("width_mm") ?></td>
                    </tr>
              <?php  endwhile; ?>
                    </tbody>
                  </table>
    </div>

      </div>
    <?php    } ?>
</section>
<section class="system-page__resources">
  <h3 class="section-title">
    Resources
  </h3>
  <div class="system-page__resource-container flex-container flex-wrap space-between">
    <?php
        if( have_rows('resources') ):
            while ( have_rows('resources') ) : the_row();
            $name = get_sub_field("name");
            $icon = get_sub_field("icon");
            $url = get_sub_field("url");
            $is_video = get_sub_field("is_video");
            if($is_video =="false"){
            ?>
              <a  href="<?php echo $url; ?>" target="_blank" rel="noopener noreferrer" class="system-page__resource flex-container align-center">
                <img src="<?php echo $icon; ?>" alt="<?php echo $name; ?> icon">
                <?php echo $name; ?>
            </a>
          <?php }else{ ?>
            <button type="button" name="button" class="system-page__resource flex-container align-center" data-open-modal data-modal="video" data-id="<?php the_field("video",8); ?>" data-source="<?php the_field("video_hosting",8); ?>">
              <img src="<?php echo $icon; ?>" alt="<?php echo $name; ?> icon">
              <?php echo $name; ?>
            </button>
          <?php  } ?>
      <?php

          endwhile;
        endif;
        ?>

  </div>
</section>
<a href="/quote" class="cta full-width "><span>Request a Free Quote</span></a>
<div class="media-modal" data-modal="video">
  <button type="button" name="button" class="media-modal__close" data-close-modal>close</button>
   <iframe class="media-modal__video" src=""></iframe>
</div>
<?php get_footer();
